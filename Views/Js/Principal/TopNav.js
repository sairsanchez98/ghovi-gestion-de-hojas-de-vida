var nav = new Vue({
    el: "#nav",
    data: {
        tabs:[
            
            {"tab":"panel_de_control", "active":""},// admin - cliente:
            {"tab":"informes", "active":""},// admin 
            {"tab":"usuarios", "active":""},// admin
            {"tab":"ofertas_empleo", "active":""},// admin - cliente:
            {"tab":"formacion", "active":""},// admin - cliente:
            {"tab":"cursos", "active":""},// admin - cliente:
            {"tab":"avisos", "active":""},// admin 

            
            {"tab":"hoja_de_vida", "active":""},// cliente:
            {"tab":"experiencia", "active":""},// cliente:
            {"tab":"aprender_mas", "active":""},// cliente:

        ]


    },
    
    methods: {
        
        // CERRAR SESIÓN DEL USUARIO LOGUEADO:
        destroySession : function (){
            axios.get(SERVERURL+"Controllers/CtrlLogin.php?destroySession")
            .then(function(response){
                console.log(response);
                let php = response.data;
                if(php.session == "destroy"){
                    window.location=SERVERURL;
                }
            })
        },

        //HOVER MENU:
        hoverMenu: function (tab){
            for (let index = 0; index < this.tabs.length; index++) {
                if(tab == nav.tabs[index]["tab"]){
                    nav.tabs[index]["active"] = "active";
                }
            }
        },

        OutHoverMenu: function (tab){
            for (let index = 0; index < this.tabs.length; index++) {
                if(tab == nav.tabs[index]["tab"]){
                    nav.tabs[index]["active"] = "";
                }
            }
        }
        
        
    },
})