var Editp = new Vue ({
    el: "#editarPerfil",
    data: {
        /// DATOS DEL PERFIL
        nombres: "",
        apellidos:"",
        correo : "",
        foto : "",

        // variables de procesamiento
        procesando : false
    },
    methods: {
        CargarInformacionActual :function (){
            axios.get(SERVERURL + "Controllers/CtrlUsuario.php?cargarInformacionActual")
            .then(function(response){
                let php = response.data;
                Editp.nombres = php.respuesta["nombre"];
                Editp.apellidos = php.respuesta["apellidos"];
                Editp.correo = php.respuesta["email"];
            })
        },

        EditarPerfil : function (){
            this.procesando = true;
            var Foto = document.querySelector('#foto'); 
            var _Foto_ = Foto.files[0];
            this.foto = _Foto_;
            // console.log(this.foto);
            let formdata = new FormData();
            formdata.append("EditarPerfil" , true);
            formdata.append("nombres",this.nombres);
            formdata.append("apellidos" , this.apellidos);
            formdata.append("correo", this.correo);
            formdata.append("foto" , this.foto);

            axios.post(SERVERURL+"Controllers/CtrlUsuario.php" , formdata)
            .then(function(response){
                let php = response.data;
                if(php.respuesta == "nombres_null" || php.respuesta == "apellidos_null" ){
                    Editp.alerts("Los campos *NOMBRES* y *APELLIDOS* son necesarios para identificarte.", "danger");
                    Editp.procesando = false;
                }else if(php.respuesta == "preg_match_nombres" || php.respuesta == "preg_match_apellidos" ){
                    Editp.alerts("No incluyas caracteres especiales en los campos *NOMBRES* y *APELLIDOS* (solo letras) ", "danger");
                    Editp.procesando = false;
                }else if(php.respuesta == "formato_correo_novalido"){
                    Editp.alerts("No hemos reconocido el formato de tu correo electrónico, intenta con otro.", "danger");
                    Editp.procesando = false;
                }else if(php.respuesta == "email_existente"){
                    Editp.alerts("El correo electrónico que ingresaste ya se encuentra registrado, intenta con otro.", "danger");
                    Editp.procesando = false;
                }else if(php.respuesta == "FormatoDeFotoNoValido" || php.respuesta=="erroAlSubirFoto"){
                    Editp.alerts("Solo permitimos fotos con formato JPG, PNG, GIF, BMP. Tu foto no debe pesar más de 3M.", "danger");
                    Editp.procesando = false;
                }else if(php.respuesta == "Actualizacion_OK"){
                    Editp.alerts("Tu información ha sido actualizada.", "success");
                    Editp.procesando = false;
                }
                // console.log(response);
            })

        },

        alerts: function (string,type){
            if(type == "danger"){
                var title = "Error!";
                var icon = "fa fa-exclamation-circle";
                var alert_color = "#37AAB9";
            }else if(type == "success"){
                var title = "Ok!";
                var icon = "fa fa-check";
                var alert_color = "#3E98CD";
            }
             ///alert de aviso error:
             $.toast().reset('all');
             $("body").removeAttr('class').addClass("bottom-right");
             $.toast({
                heading :'<i class="jq-toast-icon '+icon+'"></i>'+ string+'<br><a href="'+SERVERURL+'perfil/" class="btn btn-outline-light btn-sm mt-10">Aceptar</a>',
                //text: '<i class="jq-toast-icon '+icon+'"></i><p>'+string+'</p>',
                position: 'bottom-right',
                loaderBg:'#185F8D',
                bgColor: alert_color,
                class: 'jq-has-icon jq-toast-dark',
                hideAfter: 5500, 
                stack: 6,
                showHideTransition: 'slide'
             });
             
             ///Fin alert de aviso error
        }
        
    },
    mounted() {
        this.CargarInformacionActual();
    },
});


$('.dropify').dropify({
    messages: {
        'default': 'Arrastra y suelta tu foto aquí o haz clic',
        'replace': 'Arrastra y suelta o haz clic para reemplazar',
        'remove':  'Quitar',
        'error':   'Uy, sucedió algo malo.'
    },
    error: {
        'fileSize': 'Tu foto pesa demasiado ({{ value }} max).',
        'minWidth': 'El ancho de la imagen es demasiado pequeño. ({{ value }}}px min).',
        'maxWidth': 'El ancho de la imagen es demasiado grande. ({{ value }}}px max).',
        'minHeight': 'El alto de la imagen es demasiado pequeño. ({{ value }}}px min).',
        'maxHeight': 'El ancho de la imagen es demasiado grande. ({{ value }}px max).',
        'imageFormat': 'El formato de imagen no está permitido. ({{ value }} unicamente).',
        'fileExtension': 'Archivo no permitido ({{ value }} unicamente).'
    }
});


