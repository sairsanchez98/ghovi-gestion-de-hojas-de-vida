var courses = new Vue({
    el: "#courses",
    data: {
        // Registro:
        titulo: "",

        // process
        procesando: false,

        // Componentes
        components : [
            {"tittle": "list" , "active": true},
            {"tittle": "form" , "active": false},
            {"tittle": "add_user_to_course" , "active": false}
            
        ],

        //add users to course
        users_add : [],
        course_selected : "",

    },
    methods: {
        SelectComponent : function (component){
            for (let index = 0; index < courses.components.length; index++) {
                if(courses.components[index]["tittle"] == component){
                    courses.components[index]["active"] = true;
                }else{
                    courses.components[index]["active"] = false;
                }
            }
        },  

        CreateCourse : function(){
            this.procesando = true;
            let formdata = new FormData();
            formdata.append("create_curso" ,  true);
            formdata.append("titulo" , courses.titulo);
            axios.post(SERVERURL+"Controllers/CtrlCourses.php" ,  formdata).then(function (response){
                
                let php = response.data;
                console.log(php);
                courses.procesando = false;
                courses.getCourses();
                courses.SelectComponent("list");
            })
        },


        getCourses : function (){
            var get_ = SERVERURL+"Controllers/CtrlCourses.php?cargar_cursos_admin";
            
            var tabla = $('#datable_courses').DataTable( {
                "ajax" : get_,
                "responsive": true,
                "bPaginate": true,
                "info":     true,
                "order": [[ 1, "desc" ]],
                "columns": [
                    { "data": 4 },
                    { "data": 3 },
                    { "data": 5 }
                    
                    
                ],
                // más rapidez: 
                "deferRender":true,
                "retrieve" : true,
                "processing": true,
                "language": {
                    "sProcessing":     "Cargando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "(Vacío) Aún no se han registrado cursos",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            } );
            tabla.ajax.reload();
             
            // capturar el evento EDITAR USUARIO:
              $("#datable_courses tbody").on("click" , "button.add_participante",  function(){
                 courses.SelectComponent("edit_user");
                 var data  = tabla.row($(this).parents("tr")).data();
                 courses.course_selected = data;
                 courses.SelectComponent("add_user_to_course");
                 courses.GetUsers();
             })

            // // capturar el evento ADD COURSE:
            // $("#datable_courses tbody").on("click" , "button.addcourse",  function(){
            //     courses.SelectComponent("addcourse");
            //     var data  = tabla.row($(this).parents("tr")).data();
            //     courses.user_selected = data;
            // })
            
        },


        GetUsers  :  function (){
            var get_ = SERVERURL+"Controllers/CtrlUsuario.php?cargar_usuarios";
            var tabla = $('#datable_usuarios').DataTable( {
                "ajax" : get_,
                "responsive": true,
                "bPaginate": true,
                "info":     true,
                "order": [[ 1, "desc" ]],
                "columns": [
                    { "data": 6 },
                    { "data": 5 },
                    { "data": 2 },
                    { "data": 1 }
                ],
                // más rapidez: 
                "deferRender":true,
                "retrieve" : true,
                "processing": true,
                "language": {
                    "sProcessing":     "Cargando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "(Vacío) Aún no se ha registrado la formación",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            } );
            tabla.ajax.reload();

            $("#datable_usuarios tbody").on("click" , "tr",  function(){
                var data = tabla.row( this ).data();
                
                courses.users_add.push(data);
            })
        },

        AddUserToCourse : function AddUserToCourse(){
            this.procesando = true;
            let formdata = new FormData();
            formdata.append("create_user_y_matricular" , true);
            formdata.append("courseid" , courses.course_selected[2]);
            formdata.append("username" , courses.users_add[0][3]+courses.users_add[0][1]+courses.users_add[0][0]);
            formdata.append("password" , "981129()//Sair98");
            formdata.append("firstname" , courses.users_add[0][3]);
            formdata.append("lastname" , courses.users_add[0][4]);
            formdata.append("email" , courses.users_add[0][2]);
            formdata.append("auth" , "manual");
            formdata.append("rol" , courses.users_add[0][1]);
            axios.post(SERVERURL+"Controllers/CtrlCourses.php" , formdata).then(function(response){
                let php  = response.data;
                console.log(php.respuesta);
                courses.procesando = false;
            })
        }


        


    },
    mounted() {
        this.getCourses();
    },
})