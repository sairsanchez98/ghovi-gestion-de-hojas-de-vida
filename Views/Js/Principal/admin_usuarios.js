

$('.dropify').dropify({
    messages: {
        'default': 'Arrastra y suelta la foto del usuario aquí o haz clic',
        'replace': 'Arrastra y suelta o haz clic para reemplazar',
        'remove':  'Quitar',
        'error':   'Uy, sucedió algo malo.'
    },
    error: {
        'fileSize': 'Tu foto pesa demasiado ({{ value }} max).',
        'minWidth': 'El ancho de la imagen es demasiado pequeño. ({{ value }}}px min).',
        'maxWidth': 'El ancho de la imagen es demasiado grande. ({{ value }}}px max).',
        'minHeight': 'El alto de la imagen es demasiado pequeño. ({{ value }}}px min).',
        'maxHeight': 'El ancho de la imagen es demasiado grande. ({{ value }}px max).',
        'imageFormat': 'El formato de imagen no está permitido. ({{ value }} unicamente).',
        'fileExtension': 'Archivo no permitido ({{ value }} unicamente).'
    }
});

var user = new Vue({
    el: "#usuarios",
    data: {
        // datos de registro:
        correo: "",
        nombres: "",
        apellidos : "",
        rol: "",
        foto: "",
        contrasena: "",
        user_selected: "",
        // Procesarmiento: 
        procesando: false,

        //Componentes o vistas
        components : [
            {"tittle": "list" , "active": true},
            {"tittle": "form" , "active": false},
            {"tittle": "addcourse" , "active": false},
            {"tittle": "edit_user" , "active": false}
        ]

    },
    
    methods: {

        SelectComponent : function (component){
            for (let index = 0; index < user.components.length; index++) {
                if(user.components[index]["tittle"] == component){
                    user.components[index]["active"] = true;
                }else{
                    user.components[index]["active"] = false;
                }
            }
        },



        CreateUser:function (){
            this.procesando = true;

            var Foto = document.querySelector('#foto'); 
            var _Foto_ = Foto.files[0];
            user.foto = _Foto_;

            let formdata = new FormData();
            formdata.append("crearUsuario" , true);
            formdata.append("nombre" , user.nombres);
            formdata.append("apellidos" , user.apellidos );
            formdata.append("correo" , user.correo );
            formdata.append("rol" ,  user.rol);
            formdata.append("contrasena" , user.contrasena );
            formdata.append("foto" , user.foto);

            axios.post(SERVERURL+"Controllers/CtrlUsuario.php" , formdata).then(function(response){
                //console.log(response);
                let php = response.data;
                if(php.respuesta == "registro_ok"){
                    user.Get_users();
                    user.SelectComponent("list");
                    user.procesando = false;
                }
            })
        },



        Get_users : function (){
            var get_ = SERVERURL+"Controllers/CtrlUsuario.php?cargar_usuarios";
            var tabla = $('#datable_usuarios').DataTable( {
                "ajax" : get_,
                "responsive": true,
                "bPaginate": true,
                "info":     true,
                "order": [[ 1, "desc" ]],
                "columns": [
                    { "data": 6 },
                    { "data": 5 },
                    { "data": 2 },
                    { "data": 1 },
                    { "data": 7 }
                ],
                // más rapidez: 
                "deferRender":true,
                "retrieve" : true,
                "processing": true,
                "language": {
                    "sProcessing":     "Cargando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "(Vacío) Aún no se ha registrado la formación",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            } );
            tabla.ajax.reload();
             
            // capturar el evento EDITAR USUARIO:
             $("#datable_usuarios tbody").on("click" , "button.editarUsuario",  function(){
                user.SelectComponent("edit_user");
                var data  = tabla.row($(this).parents("tr")).data();
                user.user_selected = data;
            })

            // capturar el evento ADD COURSE:
            $("#datable_usuarios tbody").on("click" , "button.addcourse",  function(){
                user.SelectComponent("addcourse");
                var data  = tabla.row($(this).parents("tr")).data();
                user.user_selected = data;
            })
            
        },





        // prueba API moodle
        pruebaREST : function (){
            axios.get(SERVERURL+"Controllers/moodle_web_service/users.php?token").then(function(response){
                console.log(response);
            })
        }
    },
    mounted() {
        this.Get_users();
    },
});






