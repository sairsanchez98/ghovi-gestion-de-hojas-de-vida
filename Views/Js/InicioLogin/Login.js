var login = new Vue({
    el: "#Login",
    data:{
        // VARIABLES DEL FORMULARIO INICIAR SESION
        correo: "",
        contrasena : "",

        // VARIABLES DE FORMULARIO REGISTRO DE NUEVO USUARIO CLIENTE
        correo_: "",
        nombre: "",
        apellidos:"",
        contrasena_ : "",
        // VARIABLES DE PROCESO:
        procesando: false
    },
    methods: {
        iniciarSesion : function (){
            login.procesando = true;
            let formdata = new FormData();
            formdata.append("iniciar_session" , true);
            formdata.append("correo" , this.correo);
            formdata.append("contrasena" , this.contrasena);
            axios.post(SERVERURL+"Controllers/CtrlLogin.php",formdata)
            .then(function (response){
                console.log(response);
                let php = response.data;
                if(php.respuesta == "acceso_ok"){
                    login.procesando = false;
                    window.location=SERVERURL;
                }else if(php.respuesta =="formato_correo_novalido"
                        || php.respuesta == "user_not_found"){
                    login.procesando = false;
                    login.alerts("No hemos reconocido el correo electrónico que ingresaste","danger");
                }else if(php.respuesta =="incorrect_pass"){
                    login.procesando = false;
                    login.alerts("La contraseña es incorrecta","danger");
                }
            })
        },

        RegistrarUsuario : function (){
            this.procesando = true;
            let formdata = new FormData();
            formdata.append("RegistrarUsuario" , true);
            
            formdata.append("nombre",this.nombre);
            formdata.append("apellidos",this.apellidos);
            formdata.append("correo",this.correo_);
            formdata.append("contrasena",this.contrasena_);

            axios.post(SERVERURL+"Controllers/CtrlLogin.php", formdata)
            .then(function (response){
                console.log(response);
                let php  = response.data;
                if(php.respuesta == "acceso_ok"){
                    login.procesando = false;
                    window.location=SERVERURL+"perfil/";
                }else if(php.respuesta == "nombres_null" || php.respuesta == "apellidos_null"){
                    login.alerts("Los campos *nombre* y *apellidos* son obligatorios.","danger");
                    login.procesando = false;
                }else if(php.respuesta == "apellidos_pregmatch" ||
                        php.respuesta == "nombres_pregmatch" ){
                    login.alerts("Los campos (apellidos y nombres) no pueden tener carácteres especiales, solo letras.","danger");
                    login.procesando = false;
                }else if(php.respuesta == "formato_correo_novalido"){
                    login.alerts("El correo ingresado no es válido","danger");
                    login.procesando = false;
                }else if(php.respuesta == "email_existente"){
                    login.alerts("¿Ya tienes una cuenta con este correo? Porfavor inicia sesión.","danger");
                    login.procesando = false;
                }
            })
        },

        alerts: function (string,type){
            if(type == "danger"){
                var title = "Error!";
                var icon = "fa fa-exclamation-circle";
                var alert_color = "#37AAB9";
            }else if(type == "success"){
                var title = "Ok!";
                var icon = "fa fa-check";
                var alert_color = "#37AAB9";
            }
             ///alert de aviso error:
             $.toast().reset('all');
             $("body").removeAttr('class').addClass("bottom-left");
             $.toast({
                heading :'<i class="jq-toast-icon '+icon+'"></i>'+ string,
                //text: '<i class="jq-toast-icon '+icon+'"></i><p>'+string+'</p>',
                position: 'bottom-left',
                loaderBg:'#185F8D',
                bgColor: alert_color,
                class: 'jq-has-icon jq-toast-dark',
                hideAfter: 5500, 
                stack: 6,
                showHideTransition: 'slide'
             });
             ///Fin alert de aviso error
        }
        

    },
})