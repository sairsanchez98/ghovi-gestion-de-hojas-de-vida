
<div id="nav">
        <!-- Top Navbar -->
<nav class="navbar navbar-expand-xl navbar-dark bg-primary fixed-top hk-navbar ">
    <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
    <a class="navbar-brand font-30" href="<?php echo SERVERURL;?>">
        <!-- <img width="30" class="brand-img d-inline-block" src="<?php // echo SERVERURL ; ?>Assets/dist/img/logo.svg" alt="brand" />  -->
        GHOVI
    </a>
    
    <ul class="navbar-nav hk-navbar-content ">
        
        <li class="nav-item text-white font-14 ">
            <a id="navbar_search_btn" class="nav-link nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="search"></i></span></a>
        </li>
        <li class="nav-item">
            <a id="settings_toggle_btn" class="nav-link nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="settings"></i></span></a>
        </li>
        <li class="nav-item dropdown dropdown-notifications">
            <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="feather-icon"><i data-feather="bell"></i></span><span class="badge-wrap"><span class="badge badge-brown badge-indicator badge-indicator-sm badge-pill pulse"></span></span></a>
            <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                <h6 class="dropdown-header">Notifications <a href="javascript:void(0);" class="">View all</a></h6>
                <div class="notifications-nicescroll-bar">
                    <a href="javascript:void(0);" class="dropdown-item">
                        <div class="media">
                            <div class="media-img-wrap">
                                <div class="avatar avatar-sm">
                                    <img src="dist/img/avatar1.jpg" alt="user" class="avatar-img rounded-circle">
                                </div>
                            </div>
                            <div class="media-body">
                                <div>
                                    <div class="notifications-text"><span class="text-dark text-capitalize">Evie Ono</span> accepted your invitation to join the team</div>
                                    <div class="notifications-time">12m</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="javascript:void(0);" class="dropdown-item">
                        <div class="media">
                            <div class="media-img-wrap">
                                <div class="avatar avatar-sm">
                                    <img src="dist/img/avatar2.jpg" alt="user" class="avatar-img rounded-circle">
                                </div>
                            </div>
                            <div class="media-body">
                                <div>
                                    <div class="notifications-text">New message received from <span class="text-dark text-capitalize">Misuko Heid</span></div>
                                    <div class="notifications-time">1h</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="javascript:void(0);" class="dropdown-item">
                        <div class="media">
                            <div class="media-img-wrap">
                                <div class="avatar avatar-sm">
                                    <span class="avatar-text avatar-text-primary rounded-circle">
                                            <span class="initial-wrap"><span><i class="zmdi zmdi-account font-18"></i></span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="media-body">
                                <div>
                                    <div class="notifications-text">You have a follow up with<span class="text-dark text-capitalize"> Brunette head</span> on <span class="text-dark text-capitalize">friday, dec 19</span> at <span class="text-dark">10.00 am</span></div>
                                    <div class="notifications-time">2d</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="javascript:void(0);" class="dropdown-item">
                        <div class="media">
                            <div class="media-img-wrap">
                                <div class="avatar avatar-sm">
                                    <span class="avatar-text avatar-text-success rounded-circle">
                                            <span class="initial-wrap"><span>A</span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="media-body">
                                <div>
                                    <div class="notifications-text">Application of <span class="text-dark text-capitalize">Sarah Williams</span> is waiting for your approval</div>
                                    <div class="notifications-time">1w</div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="javascript:void(0);" class="dropdown-item">
                        <div class="media">
                            <div class="media-img-wrap">
                                <div class="avatar avatar-sm">
                                    <span class="avatar-text avatar-text-warning rounded-circle">
                                            <span class="initial-wrap"><span><i class="zmdi zmdi-notifications font-18"></i></span></span>
                                    </span>
                                </div>
                            </div>
                            <div class="media-body">
                                <div>
                                    <div class="notifications-text">Last 2 days left for the project</div>
                                    <div class="notifications-time">15d</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </li>
        <li class="nav-item dropdown dropdown-authentication">
            <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media">
                    <div class="media-img-wrap">
                        <div class="avatar">
                            <img src="<?php  echo SERVERURL.$USER_LOG["foto"]; ?>" alt="user" class="avatar-img img rounded-circle">
                        </div>
                        <!-- <span class="badge badge-success badge-indicator"></span> -->
                    </div>
                    <div class="media-body">
                        <span><?php echo $USER_LOG["nombre"]. " ". $USER_LOG["apellidos"]; ?><i class="zmdi zmdi-chevron-down"></i></span>
                    </div>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                <a class="dropdown-item" href="<?php echo SERVERURL;?>perfil/"><i class="dropdown-icon zmdi zmdi-account"></i><span>Perfil</span></a>
                <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-card"></i><span>Mi balance</span></a>
                <a class="dropdown-item" href="inbox.html"><i class="dropdown-icon zmdi zmdi-email"></i><span>Mis cursos</span></a>
                <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-settings"></i><span>Cambiar contraseña</span></a>
                
                <div class="dropdown-divider"></div>
                <div class="sub-dropdown-menu show-on-hover">
                    <a href="#" class="dropdown-toggle dropdown-item no-caret"><i class="zmdi zmdi-check text-success"></i>Online</a>
                    <div class="dropdown-menu open-left-side">
                        <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-check text-success"></i><span>Online</span></a>
                        <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-circle-o text-warning"></i><span>Busy</span></a>
                        <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-minus-circle-outline text-danger"></i><span>Offline</span></a>
                    </div>
                </div>
                <div class="dropdown-divider"></div>
                <a @click="destroySession()" class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-power"></i><span>Cerrar sesión</span></a>
            </div>
        </li>
    </ul>
</nav>
        <form role="search" class="navbar-search">
            <div class="position-relative">
                <a href="javascript:void(0);" class="navbar-search-icon"><span class="feather-icon"><i data-feather="search"></i></span></a>
                <input type="text" name="example-input1-group2" class="form-control" placeholder="Type here to Search">
                <a id="navbar_search_close" class="navbar-search-close" href="#"><span class="feather-icon"><i data-feather="x"></i></span></a>
            </div>
        </form>
        <!-- /Top Navbar -->

<!--Horizontal Nav-->
<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-row nav nav-tabs nav-tabs-primary"  role="tablist">
                <li class="nav-item " @mouseover="hoverMenu('panel_de_control')" @mouseout="OutHoverMenu('panel_de_control')">
                    <a :class="['nav-link', tabs[0]['active']]" href="<?php echo SERVERURL;?>" 
                        class="<?php if(!isset($_GET["_RuTa_"])){echo "active";} ?>" >
                        <span class="feather-icon"><i data-feather="airplay"></i></span>
                        <span class="nav-link-text">Panel de control</span>
                    </a>
                </li>
                <li class="nav-item" @mouseover="hoverMenu('informes')" @mouseout="OutHoverMenu('informes')">
                    <a :class="['nav-link', tabs[1]['active']]" href="<?php echo SERVERURL;?>" >
                        <span class="feather-icon"><i data-feather="activity"></i></span>
                        <span class="nav-link-text">Informes</span>
                    </a>
                </li>
                <li class="nav-item" @mouseover="hoverMenu('usuarios')" @mouseout="OutHoverMenu('usuarios')">
                    <a :class="['nav-link', tabs[2]['active']]" href="<?php echo SERVERURL;?>usuarios/"
                        class="<?php if(explode("/",@$_GET["_RuTa_"])[0]=='usuarios'){echo 'active';}?>">
                        <span class="feather-icon"><i data-feather="users"></i></span>
                        <span class="nav-link-text">Usuarios</span>
                        <!-- <span class="badge badge-success badge-sm badge-pill">v 1.0</span> -->
                    </a>
                </li>
                
                <li class="nav-item" @mouseover="hoverMenu('ofertas_empleo')" @mouseout="OutHoverMenu('ofertas_empleo')">
                    <a :class="['nav-link', tabs[3]['active']]" href="#" >
                        <span class="feather-icon"><i data-feather="briefcase"></i></span>
                        <span class="nav-link-text">Ofertas de empleo</span>
                    </a>
                </li>
                <li class="nav-item" @mouseover="hoverMenu('formacion')" @mouseout="OutHoverMenu('formacion')">
                    <a :class="['nav-link', tabs[4]['active']]" href="#" >
                        <span class="feather-icon"><i data-feather="book"></i></span>
                        <span class="nav-link-text">Formación</span>
                    </a>
                </li>
                <li class="nav-item" @mouseover="hoverMenu('cursos')" @mouseout="OutHoverMenu('cursos')">
                    <a :class="['nav-link', tabs[5]['active']]" href="<?php echo SERVERURL;?>cursos/" 
                    class="<?php if(explode("/",@$_GET["_RuTa_"])[0]=='cursos'){echo 'active';}?>">
                        <span class="feather-icon"><i data-feather="book-open"></i></span>
                        <span class="nav-link-text">Cursos</span>
                    </a>
                </li>
                <li class="nav-item" @mouseover="hoverMenu('avisos')" @mouseout="OutHoverMenu('avisos')">
                    <a :class="['nav-link', tabs[6]['active']]" href="#" >
                        <span class="feather-icon"><i data-feather="bell"></i></span>
                        <span class="nav-link-text">Avisos</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<!--/Horizontal Nav-->
</div>

<script src="<?php echo SERVERURL; ?>Views/Js/Principal/TopNav.js"></script>