 <!-- HK Wrapper -->
 

    <!-- Main Content -->
    <div class="hk-pg-wrapper hk-auth-wrapper" style= "overflow-y:hidden;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 pt-12">
                    <div class="auth-form-wrap pt-xl-0 pt-70">
                        <div class="auth-form w-xl-25 w-sm-50 w-100">
                            <a class="auth-brand text-center d-block mb-45" href="#">
                                <img width="90" class="brand-img img" src="<?php echo SERVERURL; ?>Assets/dist/img/404.png" alt="404" />
                            </a>
                            
                                <h1 class="display-4 mb-10 text-center">Error 404!</h1>
                                <p class="mb-30 text-center">No hemos encontrado lo que buscas.</p>
                                
                                    <div class="text-center">
                                       <a href="./" class="btn btn-primary btn-rounded btn-block"> Volver al inicio</a>
                                    </div>
                                
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Main Content -->


<!-- /HK Wrapper -->