 <!-- Main Content -->
 <div class="hk-pg-wrapper" id="formacion">
            <!-- Container -->
            <div class="container mt-20">
                <!-- Row -->
                <div class="row">
                    <div class="col-xl-12">
                        <section class="hk-sec-wrapper">
                            <div class="media align-items-center">
                                <div class="media-img-wrap d-flex mr-10">
                                    <div class="avatar avatar-sm">
                                        <img src="<?php echo SERVERURL;?>Assets/dist/img/iconos/experiencia.png" alt="user" class="avatar-img  img">
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class=" font-weight-500 text-dark">Hoja de vida > Experiencia</div>
                                </div>
                                <a class="btn btn-primary " href="#"  role="button">
                                    Agregar experiencia
                                </a>
                                    
                            </div>
                            
                                
                            <hr>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="table-wrap">
                                        <table id="datable_experiencia" class="table  table-hover w-100 display pb-30">
                                        
                                            <thead>
                                                <tr>
                                                    <th>Empresa o entidad</th>
                                                    <th>Cargo</th>
                                                    <th>Empleo actual</th>
                                                    <th>Fecha ingreso</th>
                                                    <th>Fecha salida</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- /Row -->
            </div>
            <!-- /Container -->
        </div>
        <!-- /Main Content -->
  
    <!-- plugins Data Table JavaScript -->
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/dist/js/dataTables-data.js"></script>
    
    <script src="<?php echo SERVERURL;?>Views/Js/Principal/experiencia.js"></script>


