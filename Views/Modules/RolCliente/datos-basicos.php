
    
 

    <!-- Main Content -->
    <div class="hk-pg-wrapper">
        <!-- Container -->
        <div class="container-fluid">
            <!-- Row -->
            <div class="row">
                <div class="col-xl-12 pa-0">
                   
                    <div class="tab-content mt-sm-60 mt-30">
                        <div class="tab-pane fade show active" role="tabpanel">
                            <div class="container">
                                <div class="hk-row">
                                    <div class="col-lg-8">
                                        <div class="card card-profile-feed">
                                            <div class="card-header card-header-action">
                                                <div class="media align-items-center">
                                                    <div class="media-img-wrap d-flex mr-10">
                                                        <div class="avatar avatar-sm">
                                                            <img src="<?php echo SERVERURL;?>Assets/dist/img/iconos/icono-hoja-de-vida.png" alt="user" class="avatar-img  img">
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class=" font-weight-500 text-dark">Hoja de vida > Datos básicos</div>
                                                    </div>
                                                </div>
                                                <!-- <div class="d-flex align-items-center card-action-wrap">
                                                    <div class="inline-block dropdown">
                                                        <a class="dropdown-toggle btn btn-primary " data-toggle="dropdown" href="#" aria-expanded="false" role="button">
                                                            Seleccionar sección
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="#">Action</a>
                                                            <a class="dropdown-item" href="#">Another action</a>
                                                            <a class="dropdown-item" href="#">Something else here</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item" href="#">Separated link</a>
                                                        </div>
                                                    </div>
                                                </div>  -->
                                            </div>
                                           
                                            <!-- ###################
                                            FORMULARIO HOJA DE VIDA
                                            ########################### -->
                                            <div class="card-body">
                                            
                                                <div class="row">
                                                    <div class="col-sm">


                                                        <!-- ACORDEON COLLAPSE -->
                                                        <form>
                                                            <div class="accordion" id="accordion_1">
                                                                
                                                                <!-- DATOS PERSONALES -->
                                                                <div class="card">
                                                                    <div class="card-header d-flex justify-content-between activestate">
                                                                        <a role="button" data-toggle="collapse" href="#DatosPersonales" aria-expanded="true">Información personal</a>
                                                                    </div>
                                                                    <div id="DatosPersonales" class="collapse show card-body" data-parent="#accordion_1" role="tabpanel">
                                                                        <!-- DATOS PERSONALES -->
                                                                        <div class="row">
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="firstName">Nombre(s)</label>
                                                                                <input class="form-control" id="firstName" placeholder="" value="" type="text">
                                                                            </div>
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="lastName">Apellidos</label>
                                                                                <input class="form-control" id="lastName" placeholder="" value="" type="text">
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="tidentificacion">Tipo de identificación</label>
                                                                                <select class="form-control custom-select d-block w-100" id="tidentificacion">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option>Cédula de ciudadanía</option>
                                                                                    <option>Cédula de extranjería</option>
                                                                                    <option>Tarjeta de identidad</option>
                                                                                    <option>Pasaporte</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="nidentificacion">N. identificación</label>
                                                                                <input class="form-control" id="nidentificacion" placeholder="ej: 123456789" type="number">
                                                                            </div>
                                                                        </div>

                                                                        

                                                                        <div class="row">
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="fechanacimiento">Fecha de nacimiento</label>
                                                                                <input class="form-control" id="fechanacimiento" type="date">
                                                                            </div>
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="genero">Género</label>
                                                                                <select class="form-control custom-select d-block w-100" id="genero">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option>Hombre</option>
                                                                                    <option>Mujer</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                        <div class="row">
                                                                            <div class="col-md-12 form-group">
                                                                                <label for="estadoCivil">Estado civil</label>
                                                                                <select class="form-control custom-select d-block w-100" id="estadoCivil">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option>Soltero(a)</option>
                                                                                    <option>Casado(a)</option>
                                                                                    <option>Separado(a)/Divorciado(a)</option>
                                                                                    <option>Viudo(a)</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="Departamento">Departamento de nacimiento</label>
                                                                                <select class="form-control custom-select d-block w-100" id="Departamento">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option>Córdoba</option>
                                                                                    <option>Sucre</option>
                                                                                </select>
                                                                            </div>

                                                                            <div class="col-md-6 form-group">
                                                                                <label for="ciudad">Ciudad de nacimiento</label>
                                                                                <select class="form-control custom-select d-block w-100" id="ciudad">
                                                                                    <option value="">Seleccione...</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6 form-group">
                                                                                <label for="Departamento">Departamento de residencia</label>
                                                                                <select class="form-control custom-select d-block w-100" id="Departamento">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option>Córdoba</option>
                                                                                    <option>Sucre</option>
                                                                                </select>
                                                                            </div>

                                                                            <div class="col-md-6 form-group">
                                                                                <label for="ciudad">Ciudad de residencia</label>
                                                                                <select class="form-control custom-select d-block w-100" id="ciudad">
                                                                                    <option value="">Seleccione...</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label for="address2">Dirección<span class="text-muted">*</span></label>
                                                                            <input class="form-control" id="address2" type="text">
                                                                        </div>

                                                                        <hr>

                                                                        <div class="row">
                                                                            <div class="col-xl-4 col-md-4 col-sm-4 col-xs-4 form-group">
                                                                                <label for="telefono">Teléfono</label>
                                                                                <select class="form-control custom-select d-block w-100" id="telefono">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option>Celular</option>
                                                                                    <option>Fijo</option>
                                                                                </select>
                                                                            </div>
                                                                            
                                                                            <div class="col-xl-2 col-md-2 col-sm-2 col-xs-2   form-group">
                                                                                <label for="indicativo">Indicativo</label>
                                                                                <input class="form-control" id="indicativo" placeholder="" value="" type="text">
                                                                            </div>
                                                                            <div class=" col-xl-6 col-md-6 col-sm-6 col-xs-6  form-group">
                                                                                <label for="numero_telefono">Número</label>
                                                                                <input class="form-control" id="numero_telefono" placeholder="" value="" type="text">
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-xl-4 col-md-4 col-sm-4 col-xs-4 form-group">
                                                                                <label for="telefono2">Teléfono 2</label>
                                                                                <select class="form-control custom-select d-block w-100" id="telefono2">
                                                                                    <option value="">Seleccione...</option>
                                                                                    <option>Celular</option>
                                                                                    <option>Fijo</option>
                                                                                </select>
                                                                            </div>
                                                                            
                                                                            <div class="col-xl-2 col-md-2 col-sm-2 col-xs-2   form-group">
                                                                                <label for="indicativo2">Indicativo </label>
                                                                                <input class="form-control" id="indicativo2" placeholder="" value="" type="text">
                                                                            </div>
                                                                            <div class=" col-xl-6 col-md-6 col-sm-6 col-xs-2  form-group">
                                                                                <label for="numero_telefono2">Número</label>
                                                                                <input class="form-control" id="numero_telefono2" placeholder="" value="" type="text">
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    
                                                                        <hr></hr>


                                                                        

                                                                        
                                                                    
                                                                        <!-- END DATOS PERSONALES -->
                                                                    </div>
                                                                </div>

                                                                <!-- PERFIL PROFESIONAL -->
                                                                <div class="card">
                                                                    <div class="card-header d-flex justify-content-between">
                                                                        <a class="collapsed" role="button" data-toggle="collapse" href="#PerfilProfesional" aria-expanded="false">Perfil profesional  </a>
                                                                    </div>
                                                                    <div id="PerfilProfesional" class="collapse" data-parent="#accordion_1">
                                                                        <div class="card-body pa-15"> 
                                                                            <div class="row">
                                                                                <div class="col-md-12 form-group">
                                                                                    <label for="cargo_deseado">Cargo o titulo para su hoja de vida</label>
                                                                                    <input class="form-control" id="cargo_deseado" placeholder="" value="" type="text">
                                                                                    
                                                                                    <label class="mt-10" for="perfil">Descripción breve de su perfil profesional</label>
                                                                                    <textarea id="perfil" class="form-control " rows="5">
                                                                                    </textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div> 
                                                                    </div>
                                                                </div>


                                                                <!-- Preferencias de empleo -->
                                                                <div class="card">
                                                                    <div class="card-header d-flex justify-content-between">
                                                                        <a class="collapsed" role="button" data-toggle="collapse" href="#preferenciasDeEmpleo" aria-expanded="false">Preferencias de empleo</a>
                                                                    </div>
                                                                    <div id="preferenciasDeEmpleo" class="collapse" data-parent="#accordion_1">
                                                                        <div class="card-body pa-15"> 
                                                                            <div class="row">
                                                                                <div class="col-md-6 form-group">
                                                                                    <label for="tidentificacion">Situación actual</label>
                                                                                    <select class="form-control custom-select d-block w-100" id="tidentificacion">
                                                                                        <option value="">Seleccione...</option>
                                                                                        <option>No tengo empleo</option>
                                                                                        <option>Estoy buscando empleo activamente</option>
                                                                                        <option>Soy empleado actualmente</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-6 form-group">
                                                                                    <label for="puesto_de_trabajo_d">Puesto de trabajo deseado</label>
                                                                                    <input class="form-control" id="puesto_de_trabajo_d"  type="text">
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-12 form-goup">
                                                                                    <label for="Area">Área <i class="fa fa-question-circle" data-toggle="tooltip-light" data-placement="right" title="Seleccione minimo 1 área de trabajo y máximo 3." ></i></label>
                                                                                    <select id="Area" class="select2 select2-multiple input-primary" multiple="multiple" data-placeholder="Choose">
                                                                                       
                                                                                        <option value="0">Seleccione una área</option>
                                                                                        <option value="1">Administración / Oficina</option>
                                                                                        <option value="15">Almacén / Logística / Transporte</option>
                                                                                        <option value="16">Atención a clientes</option>
                                                                                        <option value="17">CallCenter / Telemercadeo</option>
                                                                                        
                                                                                        <option value="19">Construcción y obra</option>
                                                                                        <option value="6">Contabilidad / Finanzas</option>
                                                                                        
                                                                                        <option value="2">Diseño / Artes gráficas</option>
                                                                                        <option value="7">Docencia</option>
                                                                                        <option value="8">Hostelería / Turismo</option>
                                                                                        <option value="9">Ingeniería</option>
                                                                                        <option value="4">Informática / Telecomunicaciones</option>
                                                                                        <option value="3">Investigación y Calidad</option>
                                                                                        <option value="10">Legal / Asesoría</option>
                                                                                        <option value="20">Mantenimiento y Reparaciones Técnicas</option>
                                                                                        <option value="12">Medicina / Salud</option>
                                                                                        <option value="21">Mercadotecnia / Publicidad / Comunicación</option>
                                                                                        
                                                                                        <option value="13">Recursos Humanos</option>
                                                                                        <option value="23">Servicios Generales, Aseo y Seguridad </option>
                                                                                        <option value="11">Ventas</option>
                                                                                        <option value="18">Compras / Comercio Exterior</option>
                                                                                        <option value="5">Dirección / Gerencia</option>
                                                                                        <option value="22">Producción / Operarios / Manufactura</option>

                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        
                                                            


                                                           
                                                           
                                                            
                                                            <button class="btn btn-primary mt-20" type="submit">Guardar hoja de vida</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>




                                    <!-- ###############################
                                    WIDGETS PANEL DERECHO
                                    ####################################### -->
                                    <div class="col-lg-4">
                                        <div class="card card-profile-feed">
                                            <div class="card-header card-header-action">
                                                <div class="media align-items-center">
                                                    <div class="media-img-wrap d-flex mr-10">
                                                        <div class="avatar avatar-sm">
                                                            <img src="<?php echo SERVERURL; ?>Assets/dist/img/iconos/mejor.png" alt="user" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="text-capitalize font-weight-500 text-dark">Mejora tu hoja de vida</div>
                                                        <!-- <div class="font-13">Business Manager</div> -->
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="row text-center">
                                                <div class="col-6 border-right pr-0">
                                                    <div class="pa-15">
                                                        <span class="d-block display-6 text-dark mb-5">154</span>
                                                        <span class="d-block text-capitalize font-14">Empleos</span>
                                                    </div>
                                                </div>
                                                <div class="col-6 px-0">
                                                    <div class="pa-15">
                                                        <span class="d-block display-6 text-dark mb-5">65</span>
                                                        <span class="d-block text-capitalize font-14">Cursos</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <ul class="list-group list-group-flush">
                                                <li class="list-group-item"><span><i class="ion ion-md-calendar font-18 text-light-20 mr-10"></i><span>Went to:</span></span><span class="ml-5 text-dark">Oh, Canada</span></li>
                                                <li class="list-group-item"><span><i class="ion ion-md-briefcase font-18 text-light-20 mr-10"></i><span>Worked at:</span></span><span class="ml-5 text-dark">Companey</span></li>
                                                <li class="list-group-item"><span><i class="ion ion-md-home font-18 text-light-20 mr-10"></i><span>Lives in:</span></span><span class="ml-5 text-dark">San Francisco, CA</span></li>
                                                <li class="list-group-item"><span><i class="ion ion-md-pin font-18 text-light-20 mr-10"></i><span>From:</span></span><span class="ml-5 text-dark">Settle, WA</span></li>
                                            </ul> -->
                                        </div>

                                        <div class="card">
                                            
                                            <img src="<?php  echo SERVERURL.$USER_LOG["foto"]; ?>" alt="user" 
                                            class="card-img-top avatar-img img " >
                                            
                                            
                                            <div class="card-body">
                                                <!-- <h5 class="card-title">Tu fotografía dice mucho de ti</h5>
                                                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
                                                <a href="<?php echo SERVERURL; ?>perfil/editar-perfil/" class="btn btn-primary"><i class="fa fa-image mr-1"> </i>Actualizar fotografía</a>
                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>	
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->

    </div>
    <!-- /Main Content -->

    <!-- Select2 JavaScript -->
    <script src="<?php echo SERVERURL; ?>Assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo SERVERURL; ?>/Views/Js/Principal/cliente-datos-basicos.js"></script>


    

   
	
    

   
	