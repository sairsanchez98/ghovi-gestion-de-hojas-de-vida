
        <!-- Container -->
        <div class="container mt-xl-10 mt-sm-30" id="Login"> 
            <!-- Title -->
            <div class="hk-pg-header card">
               
                <!-- FORM REGISTRO ROL ESTUDIANTE , COLEGIO O DOCENTE -->
                <div class="row card-body">
                    <div class="col-xl-6 mb-sm-50">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class= "row">
                                <div class="col-md-6 form-group">
                                    <input v-model="nombre" type="text" class="form-control rounded-input mt-15" placeholder="Nombre(s)">
                                </div>
                                <div class="col-md-6 form-group">
                                    <input v-model="apellidos" type="text" class="form-control rounded-input mt-15" placeholder="Apellidos">
                                </div>

                                <div class="col-md-12 form-group" >
                                    <input v-model="correo_" type="text" class="form-control rounded-input mt-15" placeholder="Correo electrónico">
                                    <input v-model="contrasena_" type="password" class="form-control rounded-input mt-15" placeholder="Contraseña">
                                </div>

                                <div class="col-md-12 mt-20 mb-20">
                                    <button v-if="!procesando" @click="RegistrarUsuario()" type="button" class="btn btn-rounded btn-outline-primary btn-block">Continuar</button>
                                    <button v-if="procesando" type="button" class="btn btn-rounded btn-primary  btn-block">
                                        <img class="img-fluid" width="16" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif"> Cargando
                                    </button>
                                </div>
                            </div>
                            
                            
                                
                            
                            
                            
                            

                            <center><a class="text-center content-center mb-20" href="<?php echo SERVERURL; ?>iniciar-sesion">¿Ya tienes una cuenta?</a></center>
                                    
                                
                        </div>
                    </div>

                    <div class="col-xl-6 mt-sm-20">
                        <div class="hk-row">
                            <div class="col-lg-12 col-md-12 col-sm-12 ">

                                <blockquote class="blockquote mb-0">
                                <h5  class=" justify-content" v-text="'Crea tu CV gratis'"></h5>
                                <hr>
                                    <div  class="w-100 bg-light mt-10 mb-10">
                                        <!-- <img width="60" src="<?php echo SERVERURL; ?>Assets/dist/img/iconos/editar.png" class="img-fluid mx-auto d-block img" alt="img"> -->
                                        <p class="text-justify">Registra tu hoja de vida y aplica al mejor empleo de Colombia. 
                                        Aplíque a las mejores ofertas, sea visible para miles de empresas, reciba ofertas adecuadas a su perfil, siga el estado de sus aplicaciones, compárese con los demás inscritos y realice cursos académicos que mejorarán su Hoja de vida.</p>
                                    </div>
                                    <button type="button" class="btn btn-rounded btn-info btn-block">
                                            <i class="fa fa-play"></i>
                                            ¿Ver un tutorial?
                                    </button>
                                </blockquote>
                                    
                                
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    
        <script src="<?php echo SERVERURL; ?>Views/Js/InicioLogin/Login.js"></script>