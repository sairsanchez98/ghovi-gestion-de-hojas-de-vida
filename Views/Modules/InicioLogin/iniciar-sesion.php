
    <div class="hk-wrapper" id="Login">

        <div class="container mt-30 ">
            <div class="row" >
                <div class="col-xl-6">
                    <div class="hk-row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <input v-model="correo" type="text" class="form-control rounded-input mt-15" placeholder="Correo">
                                    </div>
                                    <div class="col-md-12">
                                        <input v-model="contrasena" type="password" class="form-control rounded-input mt-15" placeholder="Contraseña">
                                    </div>
                                    
                                    <div class="col-md-12 mt-20">
                                        <button v-if="!procesando" @click="iniciarSesion()" type="button" class="btn  btn-outline-primary btn-rounded">Iniciar sesión</button>
                                        
                                        <button v-if="procesando" class="btn btn-outline-primary btn-rounded btn-wth-icon icon-wthot-bg">
                                            <span class="icon-label">
                                                <img class="img-fluid img" width="20" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif">
                                            </span>
                                            <span class="btn-text"> Cargando...</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6">
                    <div class="hk-row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="card">
                                <h5 class="card-header">¿Problemas?</h5>
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <button type="button" class="btn btn-rounded btn-info btn-block">Olvidé mi contraseña</button>
                                        <a href="<?php echo SERVERURL; ?>crear-cuenta" type="button" class="btn btn-rounded btn-info btn-block">Registrar mi Hoja de vida</a>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
    </div>
    <script src="<?php echo SERVERURL; ?>Views/Js/InicioLogin/Login.js"></script>
  