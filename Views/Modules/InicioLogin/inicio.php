

    <!-- Main Content -->
    <div class="hk-pg-wrapper">
        <!-- Container -->
        <div class="container-fluid">
            <!-- Row -->
            <div class="row">
                <div class="col-xl-12 pa-0">
                    <div class="profile-cover-wrap overlay-wrap">
                        <div class="profile-cover-img" style="background-image:url('<?php echo SERVERURL; ?>Assets/dist/img/backgroundInicioLogin.jpg')"></div>
                        <div class="bg-overlay bg-trans-dark-60"></div>
                        <div class="container profile-cover-content py-50">
                            <div class="hk-row"> 
                                <div class="col-lg-6">
                                    <div class="media align-items-center">
                                        <div class="media-img-wrap  d-flex">
                                            <div class="avatar">
                                                <img src="<?php echo SERVERURL; ?>Assets/dist/img/iconos/portfolio.svg" alt="user" class="avatar-img img">
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <div class="text-white text-capitalize display-6 mb-5 font-weight-400">Plataforma de Gestión de Hojas de Vida</div>
                                            <div class="font-14 text-white">
                                                <span class="mr-5">
                                                    <span class="font-weight-500 pr-5 counter-anim" >12450</span><span class="mr-5">Ofertas</span>
                                                </span>
                                                <span>
                                                    <span class="font-weight-500 pr-5 counter-anim">6540</span><span class="mr-5">Usuarios</span>
                                                </span>
                                                <span>
                                                    <span class="font-weight-500 pr-5 counter-anim" >1000</span><span>Cursos</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="button-list">
                                        <a href="#" class="btn btn-dark btn-wth-icon icon-wthot-bg btn-rounded"><span class="btn-text">Message</span><span class="icon-label"><i class="icon ion-md-mail"></i> </span></a>
                                        <a href="#" class="btn btn-icon btn-icon-circle btn-indigo btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                                        <a href="#" class="btn btn-icon btn-icon-circle btn-sky btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                                        <a href="#" class="btn btn-icon btn-icon-circle btn-purple btn-icon-style-2"><span class="btn-icon-wrap"><i class="fa fa-instagram"></i></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="bg-white shadow-bottom">
                        <div class="container">
                            <ul class="nav nav-light nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a href="#" class="d-flex h-60p align-items-center nav-link active">Feed</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="d-flex h-60p align-items-center nav-link">Projects</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="d-flex h-60p align-items-center nav-link">Groups</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="d-flex h-60p align-items-center nav-link">Photos</a>
                                </li>
                            </ul>
                        </div>	
                    </div>	 -->

                    <?php
                        $VIEW = CtrlTemplate::CargarRutaInicio();
                        include $VIEW;
                    ?>


                 
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->

        <!-- Footer -->
        <div class="hk-footer-wrap container">
            <footer class="footer">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <p class="d-inline-block">Follow us</p>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                        <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->
