 <!-- Main Content -->
 <div class="hk-pg-wrapper" id="usuarios">
    <!-- Container -->
    <div class="container mt-20">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
               
                <section class="hk-sec-wrapper">
                
                
                <!-- Title -->
                <div class="hk-pg-header">
                    <div>
                        <div class="media align-items-center">
                            <div class="media-img-wrap d-flex mr-10">
                                <div class="avatar avatar-sm">
                                    <img src="<?php echo SERVERURL;?>Assets/dist/img/iconos/users.png" alt="user" class="avatar-img  img">
                                </div>
                            </div>
                            <div class="media-body">
                                <div class=" font-weight-500 text-dark">Administración > Usuarios</div>
                            </div>    
                        </div>
                    </div>
					<div class="d-flex">
                        <a v-if="components[0]['active']==true" @click="SelectComponent('form')" class="btn btn-primary " href="#"  role="button">
                            <i class="fa fa-user-plus"></i> Agregar usuario
                        </a>
                        <a v-if="components[0]['active']==false" @click="SelectComponent('list')" class="btn btn-primary " href="#"  role="button">
                            <i class="fa fa-users"></i> Lista de usuarios
                        </a>
                    </div>
                </div>
                <!-- /Title -->

                   
                   
                    <hr>
                    <div class="row">
                        <div class="col-sm">

                            <!-- #################################################
                                LISTADO DE USUARIOS 
                            ######################################################-->

                            <div class="table-wrap" v-show="components[0]['active']==true">
                                <table id="datable_usuarios" class="table  table-hover w-100 display pb-30">
                                
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Usuario</th>
                                            <th>Correo</th>
                                            <th>Rol</th>
                                            <!-- <th>Fecha registro</th> -->
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    
                                </table>
                            </div>

                             <!-- #################################################
                                FORMULARIO DE REGISTRO DE USUARIOS
                            ######################################################-->
                            <div class="row"  v-show="components[1]['active']==true">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label for="nombres">Nombres</label>
                                            <input v-model="nombres" id="nombres" type="text" class="form-control rounded-input" placeholder="Nombres">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="apellidos">Apellidos</label>
                                            <input v-model="apellidos" id="apellidos" type="text" class="form-control rounded-input" placeholder="Apellidos">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="correo">Correo</label>
                                            <input v-model="correo" id="correo" type="email" class="form-control rounded-input" placeholder="Correo electrónico">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for="contrasena">Contraseña</label>
                                            <input v-model="contrasena" id="contrasena" type="password" class="form-control rounded-input" placeholder="Correo electrónico">
                                        </div>

                                        
                                        <div class="col-md-4 mt-15 form-group">
                                            <div class="custom-control custom-radio">
                                                <input v-model="rol" type="radio" name="rol" id="admin" value="admin"  class="custom-control-input">
                                                <label class="custom-control-label" for="admin">Admin</label>
                                            </div>
                                        </div>

                                        <div class="col-md-4 mt-15 form-group">
                                            <div class="custom-control custom-radio">
                                                <input v-model="rol" type="radio" name="rol" id="tutor" value="tutor" class="custom-control-input">
                                                <label class="custom-control-label" for="tutor">Tutor</label>
                                            </div>
                                        </div>

                                        <div class="col-md-4 mt-15 form-group">
                                            <div class="custom-control custom-radio">
                                                <input v-model="rol" type="radio" name="rol" id="cliente" value="cliente"  class="custom-control-input">
                                                <label class="custom-control-label" for="cliente">Cliente</label>
                                            </div>
                                        </div>
                                        
                                      
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label >Foto de perfil</label>
                                    <input type="file" id="foto" class="dropify" data-max-file-size="3M" data-allowed-file-extensions="png jpg jpeg bmp gif"/>
                                </div>

                                <div class="col-md-12 col-xl-12 col-sm-12 form-group mt-10">
                                    <button v-if="!procesando" @click="CreateUser()" class="btn btn-primary btn-rounded btn-block"><i class="fa fa-save"></i> Guardar cambios</button>
                                    <button v-if="procesando" class="btn btn-primary btn-rounded btn-block">
                                        <img class="img-fluid" width="16" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif"> Cargando
                                    </button>
                                    
                                </div>
                                
                            </div>



                             <!-- #################################################
                                FORMULARIO DE ASIGNACIÓN DE CURSOS A USUARIOS
                            ######################################################-->
                            <div class="table-wrap" v-show="components[2]['active']==true">
                                <table id="datable_usuarios" class="table  table-hover w-100 display pb-30">
                                
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Usuario</th>
                                            <th>Correo</th>
                                            <th>Rol</th>
                                            <!-- <th>Fecha registro</th> -->
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    
                                </table>
                            </div>



                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
</div>
        <!-- /Main Content -->
  
    

    <!-- plugins Data Table JavaScript -->
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    
    <!-- Dropify JavaScript -->
    <script src="<?php echo SERVERURL ; ?>Assets/vendors/dropify/dist/js/dropify.min.js"></script>
    
    <script src="<?php echo SERVERURL;?>Views/Js/Principal/admin_usuarios.js"></script>


