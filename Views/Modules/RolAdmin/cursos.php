 <!-- Main Content -->
 <div class="hk-pg-wrapper" id="courses">
    <!-- Container -->
    <div class="container mt-20">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
               
                <section class="hk-sec-wrapper">
                
                
                <!-- Title -->
                <div class="hk-pg-header">
                    <div>
                        <div class="media align-items-center">
                            <div class="media-img-wrap d-flex mr-10">
                                <div class="avatar avatar-sm">
                                    <img src="<?php echo SERVERURL;?>Assets/dist/img/iconos/courses.png" alt="course" class="avatar-img  img">
                                </div>
                            </div>
                            <div class="media-body">
                                <div class=" font-weight-500 text-dark">Administración > Cursos</div>
                            </div>    
                        </div>
                    </div>
					<div class="d-flex">
                        <a v-if="components[0]['active']==true" @click="SelectComponent('form')" class="btn btn-primary " href="#"  role="button">
                            <i class="fa fa-folder-plus"></i> Agregar curso
                        </a>
                        <a v-if="components[0]['active']==false" @click="SelectComponent('list')" class="btn btn-primary " href="#"  role="button">
                            <i class="fa fa-th-list"></i> Lista de cursos
                        </a>
                    </div>
                </div>
                <!-- /Title -->

                   
                   
                    <hr>
                    <div class="row">
                        <div class="col-sm">

                            <!-- #################################################
                                LISTADO DE USUARIOS 
                            ######################################################-->

                            <div class="table-wrap" v-show="components[0]['active']==true">
                                <table id="datable_courses" class="table  table-hover w-100 display pb-30">
                                
                                    <thead>
                                        <tr>
                                            <th>Estado</th>
                                            <th>Nombre</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    
                                </table>
                            </div>

                             <!-- #################################################
                                FORMULARIO DE REGISTRO DE CURSOS
                            ######################################################-->
                            <div class="row"  v-show="components[1]['active']==true">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label for="nombre">Nombre o titulo del curso</label>
                                            <input v-model="titulo" id="nombre" type="text" class="form-control rounded-input" >
                                        </div>
                                        
                                        
                                        <div class="col-md-4 mt-15 form-group">

                                            <button v-if="!procesando" @click="CreateCourse()" type="button" class="btn  btn-outline-primary btn-rounded btn-block">Guardar</button>
                                        
                                            <button v-if="procesando" class="btn btn-outline-primary btn-rounded btn-wth-icon icon-wthot-bg">
                                                <span class="icon-label">
                                                    <img class="img-fluid img" width="20" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif">
                                                </span>
                                                <span class="btn-text"> Cargando...</span>
                                            </button>

                                        </div>
                                        
                                      
                                    </div>
                                </div>
                              

                            </div>


                             <!-- #################################################
                                AGREGAR USUARIO A UN CURSO
                            ######################################################-->
                            <div class="row"  v-show="components[2]['active']==true">
                                <div class="col-md-6"> 
                                    <table id="datable_usuarios" class="table  table-hover w-100 display pb-30">
                                        
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Usuario</th>
                                                <th>Correo</th>
                                                <th>Rol</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        
                                    </table>
                                </div>
                                <div class="col-md-6"> 
                                    <div v-for="user in users_add" class="alert alert-info alert-wth-icon alert-dismissible fade show" role="alert">
                                        <span class="alert-icon-wrap"><i class="fa fa-user-check"></i></span>
                                        <p v-text="user[5]"></p>
                                        <a href="#" class="alert-link mt-5"><p v-text="user[1]"></p></a>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="col-md-4 mt-15 form-group">

                                        <button v-if="!procesando && users_add" @click="AddUserToCourse()" type="button" class="btn  btn-outline-primary btn-rounded btn-block">Guardar</button>

                                        <button v-if="procesando" class="btn btn-outline-primary btn-rounded btn-wth-icon icon-wthot-bg">
                                            <span class="icon-label">
                                                <img class="img-fluid img" width="20" src="<?php echo SERVERURL; ?>Assets/dist/img/loader.gif">
                                            </span>
                                            <span class="btn-text"> Cargando...</span>
                                        </button>

                                    </div>
                                </div>
                            </div>



                            



                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->
</div>
        <!-- /Main Content -->
  
    

    <!-- plugins Data Table JavaScript -->
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo SERVERURL;?>Assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    
    
    <script src="<?php echo SERVERURL;?>Views/Js/Principal/admin_cursos.js"></script>


