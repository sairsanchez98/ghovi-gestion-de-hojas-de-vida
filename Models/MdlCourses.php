<?php

require_once "config_conexion.php";

class MdlCourses{
    static public function CreateCourse($titulo , $id_course_moodle, $shortname){
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        ghov_courses (shortname, id_course_moodle, titulo) 
        VALUES (:shortname , :id_course_moodle , :titulo) ");
        
        $stmt->bindParam(":shortname", $shortname, PDO::PARAM_STR);
        $stmt->bindParam(":id_course_moodle", $id_course_moodle, PDO::PARAM_INT);
        $stmt->bindParam(":titulo", $titulo, PDO::PARAM_STR);
        
        
        if($stmt->execute()){
          return true;
        }else{
          return false;
        }
        
    }


    static public function GetCourse($item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM ghov_courses WHERE $item = '$value' ");
            $conn -> execute();
            return $conn->fetchAll();
          }else{
              if ($orden == "ASC") {
                  $conn = Conection::conectar()->prepare("SELECT * FROM ghov_courses ORDER BY $itemOrden ASC ");
                  $conn -> execute();
                  return $conn->fetchAll();
              }else{
                  $conn = Conection::conectar()->prepare("SELECT * FROM ghov_courses ORDER BY $itemOrden DESC ");
                  $conn -> execute();
                  return $conn->fetchAll();
              }
          }
    }
}