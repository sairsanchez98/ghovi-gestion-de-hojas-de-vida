<?php
    require_once"config_conexion.php";
    
    class MdlLogin{
        ## se comprueba si es un usuario personalizado o un email
        static public function ValidacionDeUsuario($usuario , $item)
        { 
            $stmt = Conection::conectar()->prepare(
                "SELECT * FROM ghov_users 
                    WHERE $item LIKE '$usuario'");
            $stmt -> execute();
            return $stmt -> fetchAll();
        }


        static public function RegistrarUsuario($nombres , $apellidos, $rol , $correo , $contrasena )
        {
            $stmt = Conection::conectar()->prepare("INSERT INTO 
            ghov_users (rol, email, contrasena, nombres, apellidos) 
            VALUES (:rol, :email, :contrasena, :nombres, :apellidos) ");
            
            $stmt->bindParam(":rol", $rol, PDO::PARAM_STR);
            $stmt->bindParam(":email", $correo, PDO::PARAM_STR);
            $stmt->bindParam(":contrasena", $contrasena, PDO::PARAM_STR);
            $stmt->bindParam(":nombres", $nombres, PDO::PARAM_STR);
            $stmt->bindParam(":apellidos", $apellidos, PDO::PARAM_STR);
            
            if($stmt->execute()){
              return true;
            }else{
              return false;
            }
            $stmt->close();   
        }


        static public function RegistrarColegio($id_usuario , $token, $nombre, $fecha_registro, $ultima_actualizacion)
        {
            $stmt = Conection::conectar()->prepare("INSERT INTO 
            rol_colegios (id_usuario , token , nombre , fecha_registro , ultima_actualizacion) 
            VALUES (:id_usuario , :token , :nombre , :fecha_registro , :ultima_actualizacion) ");
            
            $stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
            $stmt->bindParam(":token", $token, PDO::PARAM_STR);
            $stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
            $stmt->bindParam(":ultima_actualizacion", $ultima_actualizacion, PDO::PARAM_STR);
            
            if($stmt->execute()){
              return true;
            }else{
              return false;
            }
            $stmt->close();   
        }


        static public function RegistrarDocente($id_usuario , $nombre, $apellidos, $fecha_registro , $ultima_actualizacion)
        {
            $stmt = Conection::conectar()->prepare("INSERT INTO 
            rol_docentes (id_usuario , nombre , apellidos , fecha_registro , ultima_actualizacion) 
            VALUES (:id_usuario , :nombre , :apellidos , :fecha_registro , :ultima_actualizacion) ");
            
            $stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
            $stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            $stmt->bindParam(":apellidos", $apellidos, PDO::PARAM_STR);
            $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
            $stmt->bindParam(":ultima_actualizacion", $ultima_actualizacion, PDO::PARAM_STR);
            
            if($stmt->execute()){
              return true;
            }else{
              return false;
            }
            $stmt->close();  
        }


        static public function RegistrarEstudiante($id_usuario , $nombre, $apellidos, $fecha_registro , $ultima_actualizacion)
        {
            $stmt = Conection::conectar()->prepare("INSERT INTO 
            rol_estudiantes (id_usuario , nombre , apellidos, fecha_registro , ultima_actualizacion) 
            VALUES (:id_usuario , :nombre , :apellidos, :fecha_registro , :ultima_actualizacion) ");
            
            $stmt->bindParam(":id_usuario", $id_usuario, PDO::PARAM_INT);
            $stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            $stmt->bindParam(":apellidos", $apellidos, PDO::PARAM_STR);
            $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
            $stmt->bindParam(":ultima_actualizacion", $ultima_actualizacion, PDO::PARAM_STR);
            
            if($stmt->execute()){
              return true;
            }else{
              return false;
            }
            $stmt->close();  
        }


    }
?>