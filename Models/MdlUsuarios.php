<?php
require_once"config_conexion.php";

class Usuarios {


    static public function CargarInformacion($tabla, $item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  $tabla WHERE $item = $value  ");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM $tabla ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM $tabla ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }


    static public function EditarInformacion($id_usuario, $foto, $nombres, $apellidos, $correo){

        ### PRIMERO ACTUALIZAMOS LOS DATOS QUE TODOS LOS ROLES PUEDEN TENER
        ## USUARIO (CORREO Y FOFO)
        $stmt = Conection::conectar()->prepare("UPDATE ghov_users 
        SET email = :correo, apellidos = :apellidos, nombres=:nombres, foto=:foto
        WHERE id = :id");
        $stmt->bindParam(":correo",$correo, PDO::PARAM_STR);
        $stmt->bindParam(":apellidos",$apellidos, PDO::PARAM_STR);
        $stmt->bindParam(":nombres",$nombres, PDO::PARAM_STR);
        $stmt->bindParam(":foto",$foto, PDO::PARAM_STR);
        $stmt->bindParam(":id", $id_usuario, PDO::PARAM_INT);
        $stmt->execute();
   

    }

}