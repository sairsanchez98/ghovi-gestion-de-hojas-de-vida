-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-09-2020 a las 15:51:17
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ghovi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ghov_courses`
--

CREATE TABLE `ghov_courses` (
  `id` int(11) NOT NULL,
  `shortname` text NOT NULL,
  `id_course_moodle` int(11) NOT NULL,
  `titulo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ghov_users`
--

CREATE TABLE `ghov_users` (
  `id` int(11) NOT NULL,
  `rol` text NOT NULL,
  `email` text NOT NULL,
  `contrasena` text NOT NULL,
  `nombres` text NOT NULL,
  `apellidos` text NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ghov_users`
--

INSERT INTO `ghov_users` (`id`, `rol`, `email`, `contrasena`, `nombres`, `apellidos`, `foto`) VALUES
(1, 'admin', 'sairsanchez@array.com.co', '$2a$07$asxx54ahjppf45sd87a5auQ6UaXk0ubgXADwV6QSE8PFlbGPLrS1K', 'SAIR DE JESUS ', 'SANCHEZ VALDERRAMA', '1/27vJHpnW_400x400.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ghov_courses`
--
ALTER TABLE `ghov_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ghov_users`
--
ALTER TABLE `ghov_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ghov_courses`
--
ALTER TABLE `ghov_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ghov_users`
--
ALTER TABLE `ghov_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
