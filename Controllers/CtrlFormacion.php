<?php
session_start();
require_once"../Models/MdlFormacion.php";



///////////////////////////////////////
// CARGAR FORMACION DEL USUARIO CLIENTE//
///////////////////////////////////////
if (isset($_GET["cargar_formacion_user"])) {

    // preguntamos por el tipo de consulta 
    //(a nombre propio o es un tutor o admin que quiere ver la formación de otro usuario)
    if (isset($_GET["a_nombre_propio"])) {
        $id_user = $_SESSION["UserLoggedIn"]["user"]["id_usuario"];
    }else if(isset($_GET["desde_otro_perfil"])){
        $id_user = $_GET["id_usuario"];
    }

    /// PREGUNTAMOS POR EL ESTADO de la sesion DEL USUARIO : 
    if (isset($_SESSION["UserLoggedIn"]) && @$_SESSION["UserLoggedIn"]["estado"]=="ok") {
        /// realizamos la consulta en la base de datos:
        $formacion = Mdlformacion::GetFormacionUser("id_user",$id_user,"DESC","id");

        // creamos el JSON con los resultados obtenidos...
            $DatosJson = '{ "data": [ ';
            for ($i=0; $i < count($formacion) ; $i++) { 
                
                    $acciones = "<div class='btn-toolbar d-inline-block mb-25 mr-10' role='toolbar' aria-label='Toolbar with button groups'>" ;
                    $acciones .= "<button idcarrera='".$formacion[$i]["id"]."'  type='button' class='editarEstudiante btn btn-outline-light' data-toggle='tooltip' data-original-title='Ver, editar & asignar'><i class='fa fa-edit'></i></button>";
                    $acciones .= "<button idcarrera='".$formacion[$i]["id"]."'  type='button' class='eliminarEstudiante btn btn-outline-light' data-toggle='tooltip' data-original-title='Eliminar usuario'><i class='fa fa-times'></i></button>";
                    $acciones .= "</div>";
        
                    $DatosJson .= '[
                        "'.($formacion[$i]["id"]).'",
                        "'.($formacion[$i]["id_user"]).'",
                        "'.($formacion[$i]["institucion"]).'",
                        "'.($formacion[$i]["programa"]).'",
                        "'.($formacion[$i]["tipo_formacion"]).'",
                        "'.($formacion[$i]["graduado"]).'",
                        "'.($formacion[$i]["fecha_terminacion"]).'",
                        "'.($formacion[$i]["titulo_extranjero"]).'",
                        "'.($formacion[$i]["pais"] ).'",
                        "'.($formacion[$i]["nivel_educacion"] ).'",
                        "'.($formacion[$i]["archivo"]).'",
                        "'.($acciones).'"
                    ],';
                
            }
            $DatosJson = substr($DatosJson , 0 , -1); ## en los JSON no pueden terminar con una (,) y si revisamos bien en el foreach siempre va a terminar con uns (,) y por eso estoy substrayendo eso para quedar con el ultimo registro sin (,)        
            $DatosJson .= ' ] }';
            echo $DatosJson;

    }else{
        echo "no_sesion"; // el usuario debe loguearse...!!
    }
    // VAMOS A LA DB:::


   
}