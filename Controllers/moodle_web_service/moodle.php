<?php

// require_once "../Models/config_conexion.php";
// class moodle{
//     public $token ="5ca02a1d134518225a562f2f38a03fea" ;         
//     public $domainname = SERVERURL.'moodle';
//     public $serverUrl ;
// }

class Moodle {


    private $token ="5ca02a1d134518225a562f2f38a03fea" ;         
    private $domainname = 'http://localhost/ghovi-gestion-de-hojas-de-vida/moodle';
    private $serverUrl ;


    ######################################################
    ##  CREAR USUARIOS EN MOODLE ###
    ######################################################
    public function CreateUser($username , $password, $firstname , $lastname , $email , $auth){
        $this->serverUrl = $this->domainname . '/webservice/rest/server.php'. '?wstoken=' . 
        $this->token . '&wsfunction=core_user_create_users&moodlewsrestformat=json';

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->serverUrl."&users%5B0%5D%5Busername%5D=".$username."&users%5B0%5D%5Bpassword%5D=".$password."&users%5B0%5D%5Bfirstname%5D=".$firstname."&users%5B0%5D%5Blastname%5D=".$lastname."&users%5B0%5D%5Bemail%5D=".$email."&users%5B0%5D%5Bauth%5D=".$auth,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    
    
    
    ######################################################
    ##  MATRICULAR USUARIOS EN MOODLE ###
    ######################################################
    // ROL 5 = student // rol 3 = profesor con permisos de edición
    public function Matricular($roleid , $userid , $courseid){

        $this->serverUrl = $this->domainname . '/webservice/rest/server.php'. '?wstoken=' . 
        $this->token . '&wsfunction=enrol_manual_enrol_users&moodlewsrestformat=json';

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->serverUrl."&enrolments%5B0%5D%5Broleid%5D=".$roleid."&enrolments%5B0%5D%5Buserid%5D=".$userid."&enrolments%5B0%5D%5Bcourseid%5D=".$courseid,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;

    }




    ######################################################
    ##  CREAR CURSOS EN MOODLE ###
    ######################################################
    public function CreateCourse($fullname , $shortname, $category){

        $this->serverUrl = $this->domainname . '/webservice/rest/server.php'. '?wstoken=' . 
        $this->token . '&wsfunction=core_course_create_courses&moodlewsrestformat=json';

        
        
        

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->serverUrl . "&courses%5B0%5D%5Bfullname%5D=".str_replace(" ","%20",$fullname ) ."&courses%5B0%5D%5Bshortname%5D=".str_replace(" ","%20",$shortname)."&courses%5B0%5D%5Bcategoryid%5D=1",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }



    
    
    
    ######################################################
    ##  OBTENER EL TOKEN DE MOODLE ###
    ######################################################
    public function GetToken(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://localhost/ghovi-gestion-de-hojas-de-vida/moodle/login/token.php?username=sair_webservice&password=981129()//Sair98&service=web_service_sair",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        ));
    
        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
    }
}















######################################################
    ##  RECEPCIÓN DE PETICIONES ###
######################################################


if(isset($_GET["token"])){
    $moodle = new Moodle();
    $moodle->GetToken();
}



if(isset($_GET["create_user"])){
    $moodle = new Moodle();
    $moodle ->CreateUser(
        $_GET["username"],
        $_GET["password"],
        $_GET["firstname"],
        $_GET["lastname"],
        $_GET["email"],
        $_GET["auth"]
    );
}


if(isset($_GET["matricular_user"])){
    $moodle = new Moodle();
    $moodle ->Matricular(5,3,2);
}