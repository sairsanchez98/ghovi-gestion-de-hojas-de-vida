<?php
require_once"../Models/MdlLogin.php";
require_once"../Models/MdlUsuarios.php";


 require_once "../Ext/carbon/vendor/autoload.php";
 use Carbon\Carbon;
 date_default_timezone_set('America/Bogota');
 Carbon::setLocale('es');
 $fechaActual = Carbon::now()->toDateTimeString();


if (isset($_POST["iniciar_session"])) {
    sleep (1);
    
    $USUARIO = $_POST["correo"];
    $CONTRASENA = crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
    
    
    ## analizamos que los CARACTERES ingresados por el visitante en el campo de USUARIO
    ## correspondan bien y no sean caracteres especiales ...
    if (filter_var($USUARIO, FILTER_VALIDATE_EMAIL)) 
    {
         ## Validamos el usuario (verificamos si es un email): 
        $ValidarUsuario = MdlLogin::ValidacionDeUsuario($USUARIO , "email");
        if ($ValidarUsuario) { ## si el usuario fue encontrado como EMAIL entonces...
            ##procedemos a validar la contraseña
            if ($ValidarUsuario[0]["contrasena"] == $CONTRASENA) {
                ## Definir ruta de la foto del usuario:
                if ($ValidarUsuario[0]["foto"] == "") {
                    $foto = "Assets/dist/img/iconos/default_avatar.png";
                }else{
                    $foto = "Views/upload/usuarios/".$ValidarUsuario[0]["foto"];
                }

                
                $info_user = array(
                    "nombre" => $ValidarUsuario[0]["nombres"],
                    "apellidos" => $ValidarUsuario[0]["apellidos"],
                    "id_usuario" => $ValidarUsuario[0]["id"],
                    "email" => $ValidarUsuario[0]["email"],
                    "rol" => $ValidarUsuario[0]["rol"],
                    "foto"=> $foto
                );


                $REST["respuesta"] = "acceso_ok";## si el usuario y contraseña ya fueron validados
                ## y el acceso es OK entonces creamos la sesion:
                session_start();
                $USER_LOGUED = array("user" => $info_user ,  "estado"=>"ok"); 
                ## aquí he puesto en un array el id del usuario logueado para tener la info de él
                ## y también defino que el estado de la sesion es OK!
                $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION
                $REST["_session_"] = $_SESSION["UserLoggedIn"];
            }else{
                $REST["respuesta"] = "incorrect_pass";
            }
        }else{ 
            ## si  fue encontrado como usuario 
                ## mandamos una respuesta de "USUARIO NO ENCONTRADO"
                $REST["respuesta"] = "user_not_found";
        }
    }
    else
    {
        $REST["respuesta"] = "formato_correo_novalido";
    }



    header("Content-Type: application/json");
    echo json_encode($REST);   
}



if (isset($_POST["RegistrarUsuario"])) {
    sleep(1);
    ## datos generales:
    $CONTRASENA = crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
    $CORREO = $_POST["correo"];
    $ROL = "cliente";
    ####################################
    ## REGISTRANDO A UN CLIENTE: 
    ###################################
    
    $nombres_null = str_replace(" ","",$_POST["nombre"]);
    $apellidos_null = str_replace(" ","",$_POST["apellidos"]);
    
    if($nombres_null  !== ""){
        if($apellidos_null  !== ""){
            if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["nombre"])) {
                if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["apellidos"])) {
                        
                    if (filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
                        
                        // validar correo existente:
                            ## VALIDAMOS QUE EL CORREO NO ESTÉ REGISTRADO EN LA DB 
                            $EncotrarUsuario = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                                if (!$EncotrarUsuario) { // si el correo no existe 
                                        ## procedemos a registrar el usuario :
                                        $REGISTRAR_USUARIO = MdlLogin::RegistrarUsuario(
                                        $_POST["nombre"],$_POST["apellidos"],$ROL , $CORREO , $CONTRASENA
                                        );

                                    // Si se registra el usuario entonces: 
                                        # registramos la información del rol:
                                        ## para eso buscamos al usuario para registrarle el ID: 
                                    if ($REGISTRAR_USUARIO) { 
                                            $UsuarioRegistrado = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                                            
                                                
                                                $foto = "Assets/dist/img/iconos/default_avatar.png";
                                                $info_user = array(
                                                    "nombre" => $UsuarioRegistrado[0]["nombres"],
                                                    "apellidos" => $UsuarioRegistrado[0]["apellidos"],
                                                    "id_usuario" => $UsuarioRegistrado[0]["id"],
                                                    "email" => $UsuarioRegistrado[0]["email"],
                                                    "rol" => $UsuarioRegistrado[0]["rol"],
                                                    "foto"=> $foto
                                                );
                
                
                
                                                $REST["respuesta"] = "acceso_ok"; 
                                                session_start();
                                                $USER_LOGUED = array("user" => $info_user ,  "estado"=>"ok"); 
                                                ## aquí he puesto en un array el id del usuario logueado para tener la info de él
                                                ## y también defino que el estado de la sesion es OK!
                                                $_SESSION["UserLoggedIn"] = $USER_LOGUED; ## INGRESO LOS DATOS A LA SESION  
                                                                                            

                                    }else{
                                        $REST["respuesta"] = "err500";
                                    }  
                                }else{
                                    $REST["respuesta"] = "email_existente";
                                }

                    }else{
                        $REST["respuesta"] = "formato_correo_novalido";    
                    }
                        
                }else{
                    $REST["respuesta"] = "apellidos_pregmatch";
                }
            }else{
                $REST["respuesta"] = "nombres_pregmatch";
            }
        }else{
            $REST["respuesta"] = "apellidos_null";
        }
    }else{
        $REST["respuesta"] = "nombres_null";
    }
           

    header("Content-Type: application/json");
    echo json_encode($REST);   
}



if (isset($_GET["destroySession"])) {
    session_start();
    $_SESSION["UserLoggedIn"]["estado"] = false;
    session_destroy();

    $REST["session"] = "destroy";

    header("Content-Type: application/json");
    echo json_encode($REST);   
}