<?php
/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/

class CtrlTemplate{

	///=======================================
	##CARGAR PLANTILLA DEL USUARIO CLIENTE , TUTOR Y ADMIN
	///=======================================

	static public function CtrlPlantillaPrincipal()
	{
		include "Views/Template/template_principal.php";
	}	


	///=======================================
	##CARGAR PLANTILLA DE LA SECCIÓN INICIAL O PÁGINA DE ACCESO
	///=======================================
	static public function CtrlPlantillaInicioLogin(){
		include "Views/Template/TemplateInicioLogin.php";
	}




	///=======================================
	##CARGAR RUTAS CUANDO YA EL USUARIO ESTÁ LOGUEADO
	///=======================================
	static public function CargarRutaSession(){
		$USER_LOG = $_SESSION["UserLoggedIn"]["user"];
		$ROL = $USER_LOG["rol"];
		
		if (isset($_GET["_RuTa_"])) {
			// MÓDULOS CLIENTES:
			########################
			##ruta get:
			$ruta = explode("/",$_GET["_RuTa_"]);
			if ($ROL == "cliente")
			{
				## rutas/vistas permitidas:				
				$views = array(
					["perfil","editar-perfil"],
					["datos-basicos"],["panel-de-control"],["experiencia"],["aprender-mas"],
					["formacion"]
				); 	
				
				## procesar:
				if (count($ruta) == 1 || (count($ruta) == 2 && $ruta[1]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							return "Views/Modules/RolCliente/".$ruta[0].".php";
						break;
						}
						if (($view+1) == count($views)) { // si ya contó todas las páginas 
							//y ninguda es igual a la que insertó el usuario entonces error 404
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) == 2 || (count($ruta) == 3 && $ruta[2]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							if ($ruta[1] == $views[$view][1]) {
								return "Views/Modules/RolCliente/".$ruta[1].".php";
							break;
							}
						}
						if (($view+1) == count($views)) { // si ya contó todas las páginas 
							//y ninguda es igual a la que insertó el usuario entonces error 404
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) >= 3 && $ruta[2] !== "") {
					return "Views/Modules/404.php";
				}
				
			}
			
			
		
			
			// MÓDULOS TUTOR: 
			########################
			if ($ROL == "tutor")
			{
				## rutas/vistas permitidas:				
				$views = array(
					["perfil","editar-perfil"],
					["panel-de-control"],["usuarios"],["cursos"],["avisos"]
				); 	
				## procesar:
				if (count($ruta) == 1 || (count($ruta) == 2 && $ruta[1]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							return "Views/Modules/RolTutor/".$ruta[0].".php";
						break;
						}
						if (($view+1) == count($views)) { // si ya contó todas las páginas 
							//y ninguda es igual a la que insertó el usuario entonces error 404
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) == 2 || (count($ruta) == 3 && $ruta[2]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							if ($ruta[1] == $views[$view][1]) {
								return "Views/Modules/RolTutor/".$ruta[1].".php";
							break;
							}
						}
						if (($view+1) == count($views)) { // si ya contó todas las páginas 
							//y ninguda es igual a la que insertó el usuario entonces error 404
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) >= 3 && $ruta[2] !== "") {
					return "Views/Modules/404.php";
				}	
			}

			// MÓDULOS ADMIN: 
			########################
			if ($ROL == "admin")
			{
				## rutas/vistas permitidas:				
				$views = array(
					["perfil","editar-perfil"],
					["panel-de-control"],["usuarios"],["cursos"],["avisos"],["ofertas-de-empleo"]
				); 	
				## procesar:
				if (count($ruta) == 1 || (count($ruta) == 2 && $ruta[1]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							return "Views/Modules/RolAdmin/".$ruta[0].".php";
						break;
						}
						if (($view+1) == count($views)) { // si ya contó todas las páginas 
							//y ninguda es igual a la que insertó el usuario entonces error 404
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) == 2 || (count($ruta) == 3 && $ruta[2]=="" )) {
					for ($view=0; $view < count($views) ; $view++) { 
						if ($ruta[0] == $views[$view][0]) {
							if ($ruta[1] == $views[$view][1]) {
								return "Views/Modules/RolAdmin/".$ruta[1].".php";
							break;
							}
						}
						if (($view+1) == count($views)) { // si ya contó todas las páginas 
							//y ninguda es igual a la que insertó el usuario entonces error 404
							return "Views/Modules/404.php";
						}
					}
				}else if (count($ruta) >= 3 && $ruta[2] !== "") {
					return "Views/Modules/404.php";
				}
			}

			
		}else{
			if ($ROL == "cliente") {
				return "Views/Modules/RolCliente/inicio.php";
			}else if ($ROL == "admin") {
				return "Views/Modules/RolAdmin/inicio.php";
			}else if ($ROL == "tutor") {
				return "Views/Modules/RolTutor/inicio.php";
			}
		 	
		}

	}



	///=======================================
	##CARGAR RUTAS CUANDO NO HAY SESION INICIADA
	///=======================================
	static public function CargarRutaInicio(){ 
		if (!isset($_GET["_RuTa_"])) {
			return "Views/Modules/InicioLogin/no_sesion.php";
		}else if($_GET["_RuTa_"] == "iniciar-sesion" || $_GET["_RuTa_"] == "crear-cuenta"){
			return "Views/Modules/InicioLogin/".$_GET["_RuTa_"].".php";
		}
	}

}

/*=================================================================
=            SAIR SANCHEZ PROGRAMADR - WWW.ARRAY.COM.CO            =
===================================================================*/