<?php
session_start();
require_once "../Models/MdlCourses.php";
require_once "moodle_web_service/moodle.php";


if(isset($_POST["create_curso"])){
    
    $TITULO_NULL = str_replace(" ","",$_POST["titulo"]);
    if($TITULO_NULL !== ""){
        
        if ( preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ0-9 ]+$/' , $_POST["titulo"])){

            // una vez haya pasado el saneamiento y los filtros entonces: 
            //usamos web_service para añadir el nuevo curso a MOODLE:
            $ShortName = rand(100, 10000).$TITULO_NULL.rand(1, 100);
            
            $registrar = 0;
            while ($registrar == 0) { //hasta que no encuentre un shortname ÚNICO no podrá registrar el curso
                $shortName_Exist = MdlCourses::GetCourse("shortname" , $ShortName , "DESC" , "id");
                if(!$shortName_Exist){ //si el shortname no existe quiere deicir que podemos registrar
                    $registrar = 1;
                    break;
                }else{
                    $ShortName = rand(100, 10000).$TITULO_NULL.rand(1, 100);
                }
            }
            
            $moodle = new Moodle();
            $moodle_course = $moodle->CreateCourse($_POST["titulo"] ,$ShortName , "1" );

            // Ahora lo voy a registrar en la plataforma principal
            //------------
            $moodle_course_ = json_decode($moodle_course, true);
            
             $registrarCurso = MdlCourses::CreateCourse(
                  $_POST["titulo"],
                  $moodle_course_[0]["id"],
                  $moodle_course_[0]["shortname"]
             );
            

            
            $REST["respuesta"] = "registro_ok";
        }else{
            $REST["respuesta"] = "err_preg_match";
        }
        
    }else{
        $REST["respuesta"] = "titulo_null";
    }

    
    
    header("Content-Type: application/json");
    echo json_encode($REST);  
}



if(isset($_GET["cargar_cursos_admin"])){
    sleep(1);
    $USER_LOG = $_SESSION["UserLoggedIn"]["user"];
    if (isset($_SESSION["UserLoggedIn"]) && @$_SESSION["UserLoggedIn"]["estado"] === "ok" && $USER_LOG["rol"] === "admin") {

        $COURSES_DB = MdlCourses::GetCourse(null,null,"DESC","id");
        
          // creamos el JSON con los resultados obtenidos...
          $DatosJson = '{ "data": [ ';
            for ($i=0; $i < count($COURSES_DB) ; $i++) { 
                
                  

                    $acciones = "<div class='button-list'>";
                    $acciones .= "<a href='".SERVERURL."moodle/course/view.php?id=".$COURSES_DB[$i]["id_course_moodle"]."' class='addcourse btn btn-icon btn-icon-circle btn-success btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-eye'></i></span></a>";
                    $acciones .= "<button class='editarCurso btn btn-icon btn-icon-circle btn-warning btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-edit'></i></span></button>";
                    $acciones .= "<button class='add_participante btn btn-icon btn-icon-circle btn-purple btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-user-plus'></i></span></button>";
                    $acciones .= "<button class='deletecurso btn btn-icon btn-icon-circle btn-danger btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-times'></i></span></button>";
                    $acciones .= "</div>";

                    
                        
                    
                    $estado = "<i class='fa fa-circle text-success'></i>";
                    
                    
        
                    $DatosJson .= '[
                        "'.($COURSES_DB[$i]["id"]).'",
                        "'.($COURSES_DB[$i]["shortname"]).'",
                        "'.($COURSES_DB[$i]["id_course_moodle"]).'",
                        "'.($COURSES_DB[$i]["titulo"]).'",
                        "'.($estado).'",
                        "'.($acciones).'"
                    ],';
                
            }
            $DatosJson = substr($DatosJson , 0 , -1); ## en los JSON no pueden terminar con una (,) y si revisamos bien en el foreach siempre va a terminar con uns (,) y por eso estoy substrayendo eso para quedar con el ultimo registro sin (,)        
            $DatosJson .= ' ] }';
            echo $DatosJson;

    }else{
        echo "no_sesion"; // el usuario debe loguearse...!!
    }
}


if(isset($_POST["create_user_y_matricular"])){
    $moodle = new Moodle();
    $moodle_user = $moodle ->CreateUser(
        strtolower($_POST["username"]) ,
        $_POST["password"],
        $_POST["firstname"],
        $_POST["lastname"],
        $_POST["email"],
        $_POST["auth"]
    );

     if($_POST["rol"] == "tutor"){
         $id_rol = 3;
     }else{
         $id_rol = 5;
     }

    $respuesta_create_user = json_decode($moodle_user, true);

    $moodle_matricula = $moodle->Matricular($id_rol , $respuesta_create_user[0]["id"],$_POST["courseid"]);

    $REST["respuesta"] = $respuesta_create_user;

    header("Content-Type: application/json");
    echo json_encode($REST);  
}