<?php
session_start();
require_once"../Models/MdlUsuarios.php";
require_once"../Models/MdlLogin.php";


##=========================================
// CARGAR INFORMACIÓN ACTUAL
##=========================================
if (isset($_GET["cargarInformacionActual"])) {
    $REST["respuesta"] = $_SESSION["UserLoggedIn"]["user"];
    header("Content-Type: application/json");
    echo json_encode($REST);   
}



##=========================================
// CREANDO USUARIOS NUEVOS (DESDE UN ROL ADMIN)
##=========================================
if(isset($_POST["crearUsuario"])){
    sleep(1);
    $USER_LOG = $_SESSION["UserLoggedIn"]["user"];
    if (isset($_SESSION["UserLoggedIn"]) && @$_SESSION["UserLoggedIn"]["estado"] === "ok" && $USER_LOG["rol"] === "admin") {
        
        $CONTRASENA = crypt($_POST["contrasena"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        $CORREO = $_POST["correo"];

        $nombres_null = str_replace(" ","",$_POST["nombre"]);
        $apellidos_null = str_replace(" ","",$_POST["apellidos"]);
        
        if($nombres_null  !== ""){
            if($apellidos_null  !== ""){
                if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["nombre"])) {
                    if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $_POST["apellidos"])) {
                            
                        if (filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
                            
                            // validar correo existente:
                                ## VALIDAMOS QUE EL CORREO NO ESTÉ REGISTRADO EN LA DB 
                                $EncotrarUsuario = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                                    if (!$EncotrarUsuario) {             
                                        // si el correo no existe 
                                            ## procedemos a registrar el usuario :
                                            $REGISTRAR_USUARIO = MdlLogin::RegistrarUsuario(
                                            $_POST["nombre"],$_POST["apellidos"],$_POST["rol"] , $CORREO , $CONTRASENA
                                            );
    
                                        // Si se registra el usuario entonces: 
                                            # registramos la información del rol:
                                            ## para eso buscamos al usuario para registrarle el ID: 
                                        if ($REGISTRAR_USUARIO) { 
                                            $UsuarioRegistrado = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                                                
                                                
                                            ## si existe una foto 
                                            ## procesamos el archivo:
                                            $errorFoto = false;
                                            if (isset($_FILES["foto"])) {
                                                $FOTO = $_FILES["foto"];
                                                if ($FOTO["type"] == "image/jpg" || $FOTO["type"] == "image/gif"||
                                                $FOTO["type"] == "image/jpeg" || $FOTO["type"] == "image/bmp"||
                                                $FOTO["type"] == "image/png") 
                                                {
                                                    $archivo_temporal =  $FOTO["tmp_name"];
                                                    $nombre_archivo = $FOTO["name"];
                                                    $directorio = "../Views/upload/usuarios/".$UsuarioRegistrado[0]["id"]."/";
                                                    if (!file_exists($directorio)) { # cree el directorio si no existe
                                                        mkdir($directorio, 0755); // creación con permisos a la carpeta
                                                    }else { 
                                                        //si el directorio existe debemos borrar todo lo que haya dentro 
                                                        $files = glob($directorio."*"); //obtenemos todos los nombres de los ficheros
                                                            foreach($files as $file){
                                                                if(is_file($file))
                                                                unlink($file); //elimino el fichero
                                                            }

                                                    }
                                                    // guardamos el archivo en la ruta: : : 
                                                    $ruta = $directorio.$nombre_archivo;
                                                    # muevo el archivo 
                                                    if (move_uploaded_file($archivo_temporal, $ruta)) {
                                                        // registramos la url de la foto en la DB
                                                        $Url_foto_db = $UsuarioRegistrado[0]["id"]."/".$nombre_archivo;    
                                                    }else{
                                                        $REST["respuesta"] == "erroAlSubirFoto";
                                                        $errorFoto = true;
                                                    }

                                                    

                                                }else{
                                                    $REST["respuesta"] == "FormatoDeFotoNoValido";
                                                    $errorFoto = true;
                                                }
                                            }else{
                                                $Url_foto_db = "";
                                            }

                                            if (!$errorFoto) {
                                                $editar = Usuarios::EditarInformacion(
                                                    $UsuarioRegistrado[0]["id"],
                                                    $Url_foto_db ,
                                                    $_POST["nombre"],
                                                    $_POST["apellidos"],
                                                    $CORREO
                                                );
                                            }

                                        
                                                  
                    
                    
                    
                                            $REST["respuesta"] = "registro_ok"; 
                                                  
                                                                                                
    
                                        }else{
                                            $REST["respuesta"] = "err500";
                                        }  
                                    }else{
                                        $REST["respuesta"] = "email_existente";
                                    }
    
                        }else{
                            $REST["respuesta"] = "formato_correo_novalido";    
                        }
                            
                    }else{
                        $REST["respuesta"] = "apellidos_pregmatch";
                    }
                }else{
                    $REST["respuesta"] = "nombres_pregmatch";
                }
            }else{
                $REST["respuesta"] = "apellidos_null";
            }
        }else{
            $REST["respuesta"] = "nombres_null";
        }
               
    
        header("Content-Type: application/json");
        echo json_encode($REST);   


    }else{

    }
}



##=========================================
// CARGAR USUARIOS JSON
##=========================================
if (isset($_GET["cargar_usuarios"])) {
    sleep(1);
    $USER_LOG = $_SESSION["UserLoggedIn"]["user"];
    if (isset($_SESSION["UserLoggedIn"]) && @$_SESSION["UserLoggedIn"]["estado"] === "ok" && $USER_LOG["rol"] === "admin") {

        $USUARIOS_DB = Usuarios::CargarInformacion("ghov_users",null,null,"DESC","id");

          // creamos el JSON con los resultados obtenidos...
          $DatosJson = '{ "data": [ ';
            for ($i=0; $i < count($USUARIOS_DB) ; $i++) { 
                
                 
                    $acciones = "<div class='button-list'>";
                    $acciones .= "<button class='editarUsuario btn btn-icon btn-icon-circle btn-warning btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-edit'></i></span></button>";
                    $acciones .= "<button class='addcourse btn btn-icon btn-icon-circle btn-success btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-book-reader'></i></span></button>";
                    $acciones .= "<button class='deleteuser btn btn-icon btn-icon-circle btn-danger btn-icon-style-2'><span class='btn-icon-wrap'><i class='fa fa-times'></i></span></button>";
                    $acciones .= "</div>";

                    if(!empty($USUARIOS_DB[$i]["foto"])){
                        $foto = "<div class='avatar avatar-md'><img class='img img-fluid avatar-img rounded-circle' src='".SERVERURL."Views/upload/usuarios/".$USUARIOS_DB[$i]["foto"]."'></div>";
                    }else{
                        $foto = "<div class='avatar avatar-md'><img class='img img-fluid avatar-img rounded-circle' src='".SERVERURL."Assets/dist/img/iconos/default_avatar.png'></div>";
                    }
                    
        
                    $DatosJson .= '[
                        "'.($USUARIOS_DB[$i]["id"]).'",
                        "'.($USUARIOS_DB[$i]["rol"]).'",
                        "'.($USUARIOS_DB[$i]["email"]).'",
                        "'.($USUARIOS_DB[$i]["nombres"]).'",
                        "'.($USUARIOS_DB[$i]["apellidos"]).'",
                        "'.($USUARIOS_DB[$i]["nombres"]." ".$USUARIOS_DB[$i]["apellidos"]).'",
                        "'.($foto).'",
                        "'.($acciones).'"
                    ],';
                
            }
            $DatosJson = substr($DatosJson , 0 , -1); ## en los JSON no pueden terminar con una (,) y si revisamos bien en el foreach siempre va a terminar con uns (,) y por eso estoy substrayendo eso para quedar con el ultimo registro sin (,)        
            $DatosJson .= ' ] }';
            echo $DatosJson;

    }else{
        echo "no_sesion"; // el usuario debe loguearse...!!
    }

   
}





##=========================================
//EDITANDO USUARIOS (ESTUDIANTES Y DOCENTES)
##=========================================
if (isset($_POST["EditarPerfil"])) {
    sleep(1);
    $USER_LOG = $_SESSION["UserLoggedIn"]["user"];
    
    ## validar variables
    $CORREO = $_POST["correo"];
    $NOMBRE = $_POST["nombres"];
    $APELLIDOS = $_POST["apellidos"];

    $nombres_null = str_replace(" ","",$NOMBRE);
    $apellidos_null = str_replace(" ","",$APELLIDOS);
    
    if($nombres_null  !== ""){
        if($apellidos_null  !== ""){
            if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $NOMBRE)) {
                if (preg_match('/^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ ]+$/' , $APELLIDOS)) {
                    if (filter_var($CORREO, FILTER_VALIDATE_EMAIL)) {
                        
                        //validamos que el correo nuevo no se encuentre registrado
                        $EncotrarUsuario = MdlLogin::ValidacionDeUsuario($CORREO , "email");
                        // si el correo fue encontrado nos debemos fijar que sea el del usuario logueado
                        // ya que si no es el de el usuario actualmente logueado .. este le pertenece a otro 
                        $correo_existente = 0;
                        foreach ($EncotrarUsuario as $key => $us) {
                            if ($us["email"]!== $USER_LOG["email"]) {
                                $correo_existente  = 1; // 
                            break;
                            }
                        }

                        if ($correo_existente == 0) {

                            // si ya pasamos todos los filtros 
                            // entonces procedemos a actualizar la DB
                             ## si existe cambio de foto 
                            ## procesamos el archivo:
                            $errorFoto = false;
                            if (isset($_FILES["foto"])) {
                                $FOTO = $_FILES["foto"];
                                if ($FOTO["type"] == "image/jpg" || $FOTO["type"] == "image/gif"||
                                $FOTO["type"] == "image/jpeg" || $FOTO["type"] == "image/bmp"||
                                $FOTO["type"] == "image/png") 
                                {
                                    $archivo_temporal =  $FOTO["tmp_name"];
                                    $nombre_archivo = $FOTO["name"];
                                    $directorio = "../Views/upload/usuarios/".$USER_LOG['id_usuario']."/";
                                    if (!file_exists($directorio)) { # cree el directorio si no existe
                                        mkdir($directorio, 0755); // creación con permisos a la carpeta
                                    }else { 
                                        //si el directorio existe debemos borrar todo lo que haya dentro 
                                        $files = glob($directorio."*"); //obtenemos todos los nombres de los ficheros
                                            foreach($files as $file){
                                                if(is_file($file))
                                                unlink($file); //elimino el fichero
                                            }

                                    }
                                    // guardamos el archivo en la ruta: : : 
                                    $ruta = $directorio.$nombre_archivo;
                                    # muevo el archivo 
                                    if (move_uploaded_file($archivo_temporal, $ruta)) {
                                        // registramos la url de la foto en la DB
                                        $Url_foto_db = $USER_LOG['id_usuario']."/".$nombre_archivo;    
                                    }else{
                                        $REST["respuesta"] == "erroAlSubirFoto";
                                        $errorFoto = true;
                                    }

                                    

                                }else{
                                    $REST["respuesta"] == "FormatoDeFotoNoValido";
                                    $errorFoto = true;
                                }
                            }else{
                                $Url_foto_db = "";
                            }

                         if (!$errorFoto) {
                            $editar = Usuarios::EditarInformacion(
                                $USER_LOG["id_usuario"],
                                $Url_foto_db ,
                                $NOMBRE,
                                $APELLIDOS,
                                $CORREO
                            );
                            $_SESSION["UserLoggedIn"]["user"]["apellidos"] = $APELLIDOS;
                            $_SESSION["UserLoggedIn"]["user"]["nombre"] = $NOMBRE;
                            $_SESSION["UserLoggedIn"]["user"]["email"] = $CORREO;
                            if ($Url_foto_db !== "") {
                                $_SESSION["UserLoggedIn"]["user"]["foto"] = "Views/upload/usuarios/".$Url_foto_db;
                            }
                            
                            $REST["respuesta"] = "Actualizacion_OK";
                         }

                            
                        }else{
                            $REST["respuesta"]="email_existente";      
                        }

                    }else{$REST["respuesta"] = "formato_correo_novalido";}
                }else{$REST["respuesta"]="preg_match_apellidos";}
            }else{$REST["respuesta"]="preg_match_nombres";}
        }else{$REST["respuesta"]="apellidos_null";}
    }else{$REST["respuesta"]="nombres_null";}

   
     

    
     

    header("Content-Type: application/json");
    echo json_encode($REST);   

}